package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Chariot;
import ChessType.Position;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class ChariotTest {
	@Test
	public void chariot_getPossible_1() {
		//chariot pos 0 0
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		chess.setPosition(0,0);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
				
		expected.add(new Position(0,1));
		expected.add(new Position(1,0));
		expected.add(new Position(0,2));
		expected.add(new Position(0,3));
		
		TestUtil.sortPosition(expected); TestUtil.sortPosition(avaPos);
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void chariot_getPossible_2() {
		//chariot pos 8 0
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_2];
		chess.setPosition(8,0);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
				
		expected.add(new Position(8,1));
		expected.add(new Position(7,0));
		expected.add(new Position(8,2));
		expected.add(new Position(8,3));

		
		TestUtil.sortPosition(expected); TestUtil.sortPosition(avaPos);
		assertEquals(expected.toString(), avaPos.toString());
	}
		
	@Test
	public void chariot_getPossible_3() {
		//chariot pos 0 9
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_1];
		chess.setPosition(0,9);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
				
		expected.add(new Position(0,6));
		expected.add(new Position(1,9));
		expected.add(new Position(0,7));
		expected.add(new Position(0,8));

		
		TestUtil.sortPosition(expected); TestUtil.sortPosition(avaPos);
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void chariot_getPossible_4() {
		//chariot pos 8 8
		//Y axis 8 has B chariot_2 only
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2];
		chess.setPosition(8,8);

		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
				
		expected.add(new Position(0,8));
		expected.add(new Position(1,8));
		expected.add(new Position(2,8));
		expected.add(new Position(3,8));
		expected.add(new Position(4,8));
		expected.add(new Position(5,8));
		expected.add(new Position(6,8));
		expected.add(new Position(7,8));
		
		expected.add(new Position(8,6));
		expected.add(new Position(8,7));
		expected.add(new Position(8,9));

		TestUtil.sortPosition(expected); TestUtil.sortPosition(avaPos);
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void chariot_getPossible_5() {
		//chariot pos 0 1
		//Y axis 1 has A chariot_1 only
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		chess.setPosition(0,1);

		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
				
		expected.add(new Position(1,1));
		expected.add(new Position(2,1));
		expected.add(new Position(3,1));
		expected.add(new Position(4,1));
		expected.add(new Position(5,1));
		expected.add(new Position(6,1));
		expected.add(new Position(7,1));
		expected.add(new Position(8,1));
		
		expected.add(new Position(0,0));
		expected.add(new Position(0,2));
		expected.add(new Position(0,3));

		TestUtil.sortPosition(expected); TestUtil.sortPosition(avaPos);
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void chariot_validMovement_1(){
		//pos overflow -1 -1
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(0, 0);
		Position dest = new Position(-1, -1);
		
		assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
	}
	
	@Test
	public void chariot_validMovement_2(){
		//pos same
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(0, 0);
		Position dest = new Position(0, 0);
		
		assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
	}
	
	@Test
	public void chariot_validMovement_3(){
		//pos overflow -1 -1
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(0, 0);
		Position dest = new Position(0, 1);
		
		assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
	}
	
	@Test
	public void chariot_validMovement_4(){
		//pos invalid 
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(0, 0);
		Position dest = new Position(1, 1);
		
		assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
	}
	
	@Test
	public void chariot_canKillGeneral_1(){
		//pos can kill B general 
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(4, 7);
		
		assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void chariot_canKillGeneral_2(){
		//pos cannot kill B general 
		Chariot chess = new Chariot(Constants.PLAYER_A);
		chess.setPosition(0, 0);
		
		assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void chariot_canKillGeneral_3(){
		//pos cannot kill A general 
		Chariot chess = new Chariot(Constants.PLAYER_B);
		chess.setPosition(0, 0);
		
		assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void chariot_canKillGeneral_4(){
		//pos can kill A general 
		Chariot chess = new Chariot(Constants.PLAYER_B);
		chess.setPosition(4, 1);
		
		assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void chariot_goTo_1(){
		//dead chess move
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		Position dest = new Position(4,0);
		chess.die();
		
		assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
	}
	
	@Test
	public void chariot_goTo_2(){
		//valid move
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void chariot_goTo_3(){
		//invalid move, cross river
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		Position dest = new Position(4,5);
		
		assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
	}
	
	@Test
	public void chariot_goTo_4(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 2);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void chariot_goTo_5(){
		//go to dead chess pos
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		Solider enemy = (Solider)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
		enemy.setPosition(0, 2);
		enemy.die();
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void chariot_goTo_6(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
		GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
	}
	
}
