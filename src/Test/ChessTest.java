package Test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import ChessType.Chess;
import ChessType.Position;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class ChessTest {
	
    PrintStream oldPrintStream;
    ByteArrayOutputStream bos;

    private void setOutput() throws Exception {
        oldPrintStream = System.out;
        bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
    }

    private String getOutput() {// throws Exception
        System.setOut(oldPrintStream);
        return bos.toString();
    }
    
	/**************************************
     * Note: Do not modify the above part
     ***************************************/

	@Test
	public void chess_isPositionExistsAliveChess_1(){
		//Test dead chess
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
		chess.die();
		Position pos = chess.getPosition();
		assertEquals(Constants.POSITION_HAS_DEAD_CHESS, Chess.isPositionExistsAliveChess(pos));
	}
		
	@Test
	public void chess_isPositionExistsAliveChess_2(){
		//Test not dead chess
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
		Position pos = chess.getPosition();
		assertEquals(Constants.PLAYER_A, Chess.isPositionExistsAliveChess(pos));
	}
	
	@Test
	public void chess_isPositionExistsAliveChess_3(){
		//Test have chess on that pos and not dead yet
		Main.createGameObject();
		Main.initGame();
		
		assertEquals(Constants.PLAYER_A, Chess.isPositionExistsAliveChess(new Position(0,0)));
	}
	
	@Test
	public void chess_isPositionExistsAliveChess_4(){
		//Test no chess on that pos
		Main.createGameObject();
		Main.initGame();
		
		assertEquals(Constants.POSITION_HAS_NO_CHESS, Chess.isPositionExistsAliveChess(new Position(0,1)));
	}
	
	@Test
	public void chess_killEnermy_1(){
		//kill existing chess
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
		Chess.killEnermy(chess.getPosition());
		assertEquals(false, chess.isAlive());
	}
	
	@Test
	public void chess_killEnermy_2(){
		//kill non-existing chess in the chess list
		Main.createGameObject();
		Main.initGame();
		
		Chess.killEnermy(new Position(0,1));
	}
	
	@Test
	public void chess_killEnermy_3() throws Exception{
		//kill A general
		setOutput();
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
		Chess.killEnermy(chess.getPosition());
		
		assertEquals("Player 1 Lose.\nPlayer 2 Win.", getOutput().trim());
	}
	
	@Test
	public void chess_killEnermy_4() throws Exception{
		//kill B general
		setOutput();
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
		Chess.killEnermy(chess.getPosition());
		
		assertEquals("Player 1 Win.\nPlayer 2 Lose.", getOutput().trim());
	}

}
