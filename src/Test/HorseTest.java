package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Chess;
import ChessType.Horse;
import ChessType.Position;
import ChessType.Scholar;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class HorseTest {
	@Test
	public void horse_possibleMove_1() {
		//normal case for horse pos 1 0
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 0);
		
		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		
		expected.add(new Position(0,2));
		expected.add(new Position(2,2));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_2() {
		// case for horse pos 1 0 and kick foot with pos 1 1
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 0);
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		chess.setPosition(1, 1);
		
		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_3() {
		// case for horse pos 1 0 and kick foot with pos 1 1
		// and dead chess at 11
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 0);
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		chess.setPosition(1, 1);
		chess.die();
		
		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		
		expected.add(new Position(0,2));
		expected.add(new Position(2,2));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_4() {
		//normal case for horse pos 1 9 
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 9);

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		
		expected.add(new Position(0,7));
		expected.add(new Position(2,7));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}

	@Test
	public void horse_possibleMove_5() {
		// case for horse pos 1 9 with kick foot 1 8
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 9);
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
		chess.setPosition(1, 8);

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_6() {
		// case for horse pos 1 9 with kick foot 1 8
		// dead chess at 1 8
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(1, 9);
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
		chess.setPosition(1, 8);
		chess.die();

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(0, 7));
		expected.add(new Position(2, 7));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_7() {
		// case for horse pos 4 8 
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(4, 8);

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(2, 7));
		expected.add(new Position(2, 9));
		expected.add(new Position(3, 6));
		expected.add(new Position(5, 6));
		expected.add(new Position(6, 7));
		expected.add(new Position(6, 9));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_8() {
		// case for horse pos 4 8 with kick foot 5 8
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(4, 8);
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
		chess.setPosition(5, 8);
		chess.die();

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(2, 7));
		expected.add(new Position(2, 9));
		expected.add(new Position(3, 6));
		expected.add(new Position(5, 6));
		expected.add(new Position(6, 7));
		expected.add(new Position(6, 9));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_possibleMove_9() {
		// case for horse pos 5 8 with kick foot 4 8
		// dead kick foot
		Main.createGameObject();
		Main.initGame();
		
		Horse horse = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		horse.setPosition(5, 8);
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
		chess.setPosition(4, 8);
		chess.die();

		ArrayList<Position> avaList = horse.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(3, 7));
		expected.add(new Position(3, 9));
		expected.add(new Position(7, 7));
		expected.add(new Position(7, 9));
		expected.add(new Position(4, 6));
		expected.add(new Position(6, 6));
		
		TestUtil.sortPosition(expected);TestUtil.sortPosition(avaList);
		
		assertEquals(expected.toString(), avaList.toString());
	}
	
	@Test
	public void horse_validMovement_1(){
		//overflow pos
		Horse chess = new Horse(Constants.PLAYER_A);
		chess.setPosition(1,0);
		
		Position dest = new Position(-1,-1);
		assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
	}
	
	@Test
	public void horse_validMovement_2(){
		// same pos
		Horse chess = new Horse(Constants.PLAYER_A);
		chess.setPosition(1,0);
		
		Position dest = new Position(1,0);
		assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
	}
	
	@Test
	public void horse_validMovement_3(){
		//valid move
		Horse chess = new Horse(Constants.PLAYER_A);
		chess.setPosition(1,0);
		
		Position dest = new Position(0,2);
		assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
	}
	
	@Test
	public void horse_validMovement_4(){
		//invalid move with same X
		Horse chess = new Horse(Constants.PLAYER_A);
		chess.setPosition(1,0);
		
		Position dest = new Position(1,1);
		assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
	}
	
	@Test
	public void horse_validMovement_5(){
		//invalid move with same Y
		Horse chess = new Horse(Constants.PLAYER_A);
		chess.setPosition(1,0);
		
		Position dest = new Position(2,0);
		assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
	}
	
	@Test
	public void horse_canKillGeneral_1(){
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		chess.setPosition(3, 7);
		
		assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void horse_canKillGeneral_2(){
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		chess.setPosition(3, 8);
		
		assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void horse_canKillGeneral_3(){
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
		chess.setPosition(3, 2);
		
		assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
	}
	
	@Test
	public void horse_goTo_1(){
		//dead chess move
		Main.createGameObject();
		Main.initGame();
		
		Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
		Position dest = new Position(4,4);
		chess.die();
		
		assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
	}
	
	@Test
	public void horse_goTo_2(){
		//valid move
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void horse_goTo_3(){
		//invalid move, cross river
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		Position dest = new Position(4,5);
		
		assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
	}
	
	@Test
	public void horse_goTo_4(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void horse_goTo_5(){
		//go to dead chess pos
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		Solider enemy = (Solider)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
		enemy.setPosition(0, 2);
		enemy.die();
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void horse_goTo_6(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
		GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
	}
	
	

	

}
