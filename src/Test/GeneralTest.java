package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Chess;
import ChessType.General;
import ChessType.Position;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class GeneralTest {

	//Start general
		@Test
		public void general_getPossibleMove_1(){
			//Test for general for A side
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,1));
			expectedResult.add(new Position(3,0));
			expectedResult.add(new Position(5,0));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_2(){
			//Test for general, the x pos of A and B general are different
			Main.createGameObject();
			Main.initGame();
			
			General generalA = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			General generalB = (General)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
			generalA.setPosition(3, 0);
			generalB.setPosition(4, 9);
			ArrayList<Position> avaPos = generalB.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(3,9));
			expectedResult.add(new Position(4,8));
			expectedResult.add(new Position(5,9));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_3(){
			//Test for general, pos 5 0
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			chess.setPosition(5, 0);
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,0));
			expectedResult.add(new Position(5,1));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_4(){
			//Test for general A, pos 3 0
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			chess.setPosition(3, 0);
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,0));
			expectedResult.add(new Position(3,1));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_5(){
			//Test for general A, pos 4 2
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			chess.setPosition(4, 2);
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(3,2));
			expectedResult.add(new Position(4,1));
			expectedResult.add(new Position(5,2));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_6(){
			//Test for general B, pos 4 7
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
			chess.setPosition(4, 7);
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,8));
			expectedResult.add(new Position(3,7));
			expectedResult.add(new Position(5,7));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_7(){
			//Test for general A 飛將
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
			Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];
			
			solider_A_3.setPosition(5, 4);
			solider_B_3.setPosition(5, 6);
			chess.setPosition(4, 0);
			
			System.out.println(chess.getOwner());
			
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,1));
			expectedResult.add(new Position(4,9));
			expectedResult.add(new Position(3,0));
			expectedResult.add(new Position(5,0));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_getPossibleMove_9(){
			//Test for general B 飛將
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
			Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
			Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];
			
			solider_A_3.setPosition(5, 4);
			solider_B_3.setPosition(5, 6);
			chess.setPosition(4, 9);
			
			ArrayList<Position> avaPos = chess.getPossibleMove();
			ArrayList<Position> expectedResult = new ArrayList<Position>();
			expectedResult.add(new Position(4,0));
			expectedResult.add(new Position(4,8));
			expectedResult.add(new Position(3,9));
			expectedResult.add(new Position(5,9));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expectedResult);
			
			assertEquals(expectedResult.toString(), avaPos.toString());
		}
		
		@Test
		public void general_validMovement_1(){
			//test outbound pos
			General chess = new General(Constants.PLAYER_A);
			chess.setPosition(4, 0);
			Position dest = new Position(-1,-1);
			
			assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
		}
		
		@Test
		public void general_validMovement_2(){
			//test same pos
			General chess = new General(Constants.PLAYER_A);
			chess.setPosition(4, 0);
			Position dest = new Position(4,0);
			
			assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
		}
		
		@Test
		public void general_validMovement_3(){
			//valid case	
			Main.createGameObject();
			Main.initGame();
			
			General chess = new General(Constants.PLAYER_A);
			chess.setPosition(4, 0);
			Position dest = new Position(4,1);
			
			assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));	
		}
		
		@Test
		public void general_validMovement_4(){
			//invalid position case 	
			Main.createGameObject();
			Main.initGame();
			
			General chess = new General(Constants.PLAYER_A);
			chess.setPosition(4, 0);
			Position dest = new Position(5,1);
			
			assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));	
		}
		
		@Test
		public void general_canKillGeneral_1(){
			// cant kill general for A
			Main.createGameObject();
			Main.initGame();
			
			General chess = new General(Constants.PLAYER_A);
			chess.setPosition(4, 0);
			assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void general_canKillGeneral_2(){
			// cant kill general for B
			Main.createGameObject();
			Main.initGame();
			
			General chess = new General(Constants.PLAYER_B);
			chess.setPosition(4, 9);
			assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void general_canKillGeneral_3(){
			// can kill general by fly
			General chess = (General)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
			Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
			Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];
			
			solider_A_3.setPosition(5, 4);
			solider_B_3.setPosition(5, 6);
			chess.setPosition(4, 9);
			
			assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void general_goTo_1(){
			//dead chess move
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			Position dest = new Position(4,0);
			chess.die();
			
			assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
		}
		
		@Test
		public void general_goTo_2(){
			//valid move
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			Position dest = new Position(4,1);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void general_goTo_3(){
			//invalid move, cross river
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			Position dest = new Position(4,5);
			
			assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
		}
		
		@Test
		public void general_goTo_4(){
			//eat chess
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
			Position dest = new Position(4,1);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void general_goTo_5(){
			//go to dead chess pos
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			Solider enemy = (Solider)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
			enemy.setPosition(4, 1);
			enemy.die();
			Position dest = new Position(4,1);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void general_goTo_6(){
			//eat chess
			Main.createGameObject();
			Main.initGame();
			
			General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
			GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(4, 1);
			Position dest = new Position(4,1);
			
			assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
		}

}
