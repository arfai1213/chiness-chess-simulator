package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Chess;
import ChessType.Elephant;
import ChessType.Position;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class ElephantTest {

	@Test
	public void elephant_getPossibleMovement_1() {
		//Valid case with pos 2 0, no kick foot
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(0, 2));
		expected.add(new Position(4, 2));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());
	}
		
	@Test
	public void elephant_getPossibleMovement_2() {
		//elephant pos 2 0, no kick foot
		//for the checkkickfootresult that +1 +1
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		chess.setPosition(2, 0);
		Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		kickFoot.setPosition(3, 1);
				
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(0, 2));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void elephant_getPossibleMovement_3() {
		//elephant with pos 2 0, with dead kick foot
		//for the checkkickfootresult that +1 +1
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		chess.setPosition(2, 0);
		Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		kickFoot.setPosition(3, 1);
		kickFoot.die();
				
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(0, 2));
		expected.add(new Position(4, 2));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());
	}
	
	@Test
	public void elephant_getPossibleMovement_4(){
		//valid case, no kick foot case, pos 6 9
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		chess.setPosition(6, 9);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(4, 7));
		expected.add(new Position(8, 7));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	
	@Test
	public void elephant_getPossibleMovement_5() throws Exception{
		//with kick foot case
		//Test elephant pos 6 9 , with kick foot chess with pos 7 8
		Main.createGameObject();
		Main.initGame();
		
		Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		kickFoot.setPosition(7,8);
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		chess.setPosition(6, 9);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(4, 7));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_getPossibleMovement_6(){
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		chess.setPosition(6, 9);
		Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
		kickFoot.setPosition(7,8);
		kickFoot.die();
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(4, 7));
		expected.add(new Position(8, 7));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_getPossibleMovement_7(){
		//Test for elephant with pos 6 0, with kickFoot 5 1 pos
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_2];
		chess.setPosition(6, 0);
		
		Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		kickFoot.setPosition(5, 1);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(8, 2));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_getPossibleMovement_8(){
		//Test for elephant with pos 6 0, with kickFoot 5 1 pos(dead chess)
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_2];
		chess.setPosition(6, 0);
		
		Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		kickFoot.setPosition(5, 1);
		kickFoot.die();
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(8, 2));
		expected.add(new Position(4, 2));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_getPossibleMovement_9(){
		//Test for elephant with pos 2 9, with kickFoot 1 8 pos
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_1];
		chess.setPosition(2, 9);
		
		Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		kickFoot.setPosition(1, 8);
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(4, 7));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_getPossibleMovement_10(){
		//Test for elephant with pos 6 0, with kickFoot 5 1 pos(dead chess)
		Main.createGameObject();
		Main.initGame();
		
		Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_1];
		chess.setPosition(2, 9);
		
		Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
		kickFoot.setPosition(1, 8);
		kickFoot.die();
		
		ArrayList<Position> avaPos = chess.getPossibleMove();
		ArrayList<Position> expected = new ArrayList<Position>();
		expected.add(new Position(4, 7));
		expected.add(new Position(0, 7));
		
		TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
		
		assertEquals(expected.toString(), avaPos.toString());	
	}
	
	@Test
	public void elephant_validMovement_1(){
		//test overflow pos
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(-1,-1);
		
		assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_validMovement_2(){
		//test cross river
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(0, 7);
		
		assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_validMovement_3(){
		//test same pos
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(2, 0);
		
		assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_validMovement_4(){
		//test invalid move
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(0, 3);
		
		assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_validMovement_5(){
		//test valid move
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(4, 2);
		
		assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_validMovement_6(){
		//test invalid move, same X
		Elephant chess = new Elephant(Constants.PLAYER_A);
		chess.setPosition(2, 0);
		Position dest = new Position(2, 1);
		
		assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
	}
	
	@Test
	public void elephant_goTo_1(){
		//dead chess move
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		Position dest = new Position(4,0);
		chess.die();
		
		assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
	}
	
	@Test
	public void elephant_goTo_2(){
		//valid move
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void elephant_goTo_3(){
		//invalid move, cross river
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		Position dest = new Position(4,5);
		
		assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.goTo(dest));
	}
	
	@Test
	public void elephant_goTo_4(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 2);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void elephant_goTo_5(){
		//go to dead chess pos
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		Solider enemy = (Solider)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
		enemy.setPosition(0, 2);
		enemy.die();
		Position dest = new Position(0,2);
		
		assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
	}
	
	@Test
	public void elephant_goTo_6(){
		//eat chess
		Main.createGameObject();
		Main.initGame();
		
		Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
		GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
		Position dest = new Position(0,2);
		
		assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
	}
	
	
	
	
	
}
