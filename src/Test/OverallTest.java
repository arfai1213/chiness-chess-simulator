package Test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import org.junit.Test;

import ChessType.Cannon;
import ChessType.Chariot;
import ChessType.Chess;
import ChessType.Elephant;
import ChessType.General;
import ChessType.Horse;
import ChessType.Position;
import ChessType.Scholar;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.CommandHandler;
import Run.Main;

public class OverallTest {
    PrintStream oldPrintStream;
    ByteArrayOutputStream bos;

    private void setOutput() throws Exception {
        this.oldPrintStream = System.out;
        this.bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(this.bos));
    }

    private String getOutput() {// throws Exception
        System.setOut(this.oldPrintStream);
        return this.bos.toString();
    }

    /**************************************
     * Note: Do not modify the above part
     ***************************************/

    // Position
    @Test
    public void position_isInsideBoard_1() {
        // Test valid position
        Position pos = new Position(4, 4);
        assertEquals(Constants.POSITION_VALID, Position.isInsideBoard(pos));
    }

    @Test
    public void position_isInsideBoard_2() {
        // Test overflow X
        Position pos = new Position(10, 4);
        assertEquals(Constants.POSITION_X_INVALID, Position.isInsideBoard(pos));
    }

    @Test
    public void position_isInsideBoard_3() {
        // Test negative X
        Position pos = new Position(-1, 4);
        assertEquals(Constants.POSITION_X_INVALID, Position.isInsideBoard(pos));
    }

    @Test
    public void position_isInsideBoard_4() {
        // Test overflow Y
        Position pos = new Position(4, 11);
        assertEquals(Constants.POSITION_Y_INVALID, Position.isInsideBoard(pos));
    }

    @Test
    public void position_isInsideBoard_5() {
        // Test negative Y
        Position pos = new Position(4, -1);
        assertEquals(Constants.POSITION_Y_INVALID, Position.isInsideBoard(pos));
    }

    @Test
    public void position_isCrossRiver_1() {
        // For A side, Pos 4 4 is not CrossRiver
        Position pos = new Position(4, 4);
        assertEquals(false, Position.isCrossRiver(Constants.PLAYER_A, pos));
    }

    @Test
    public void position_isCrossRiver_2() {
        // For B side, Pos 4 4 is CrossRiver
        Position pos = new Position(4, 4);
        assertEquals(true, Position.isCrossRiver(Constants.PLAYER_B, pos));
    }

    @Test
    public void position_isCrossRiver_3() {
        // For A side, Pos 4 7 is CrossRiver
        Position pos = new Position(4, 7);
        assertEquals(true, Position.isCrossRiver(Constants.PLAYER_A, pos));
    }

    @Test
    public void position_isCrossRiver_4() {
        // For B side, Pos 4 7 is not CrossRiver
        Position pos = new Position(4, 7);
        assertEquals(false, Position.isCrossRiver(Constants.PLAYER_B, pos));
    }

    @Test
    public void position_isPositionInsidePossibleList_1() {
        // Pos 1 2 is inside the given list
        Position pos = new Position(1, 2);

        ArrayList<Position> list = new ArrayList<Position>();
        list.add(new Position(1, 2));
        list.add(new Position(2, 2));

        assertEquals(true, Position.isPositionInsidePossibleList(pos, list));
    }

    @Test
    public void position_isPositionInsidePossibleList_2() {
        // Pos 1 1 is not in the list, but the X is in the list
        Position pos = new Position(1, 1);

        ArrayList<Position> list = new ArrayList<Position>();
        list.add(new Position(1, 2));
        list.add(new Position(2, 2));

        assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
    }

    @Test
    public void position_isPositionInsidePossibleList_3() {
        // Pos 0 2 is not in the list, but the Y is in the list
        Position pos = new Position(0, 2);

        ArrayList<Position> list = new ArrayList<Position>();
        list.add(new Position(1, 2));
        list.add(new Position(2, 2));

        assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
    }

    @Test
    public void position_isPositionInsidePossibleList_4() {
        // Pos -1 -1 is not in the list
        Position pos = new Position(-1, -1);

        ArrayList<Position> list = new ArrayList<Position>();
        list.add(new Position(1, 2));
        list.add(new Position(2, 2));

        assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
    }

    // Chess
    @Test
    public void chess_isPositionExistsAliveChess_1() {
        // Test dead chess
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.die();
        Position pos = chess.getPosition();
        assertEquals(Constants.POSITION_HAS_DEAD_CHESS, Chess.isPositionExistsAliveChess(pos));
    }

    @Test
    public void chess_isPositionExistsAliveChess_2() {
        // Test not dead chess
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        Position pos = chess.getPosition();
        assertEquals(Constants.PLAYER_A, Chess.isPositionExistsAliveChess(pos));
    }

    @Test
    public void chess_isPositionExistsAliveChess_3() {
        // Test have chess on that pos and not dead yet
        Main.createGameObject();
        Main.initGame();

        assertEquals(Constants.PLAYER_A, Chess.isPositionExistsAliveChess(new Position(0, 0)));
    }

    @Test
    public void chess_isPositionExistsAliveChess_4() {
        // Test no chess on that pos
        Main.createGameObject();
        Main.initGame();

        assertEquals(Constants.POSITION_HAS_NO_CHESS, Chess.isPositionExistsAliveChess(new Position(0, 1)));
    }

    @Test
    public void chess_killEnermy_1() {
        // kill existing chess
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        Chess.killEnermy(chess.getPosition());
        assertEquals(false, chess.isAlive());
    }

    @Test
    public void chess_killEnermy_2() {
        // kill non-existing chess in the chess list
        Main.createGameObject();
        Main.initGame();

        Chess.killEnermy(new Position(0, 1));
    }

    @Test
    public void chess_killEnermy_3() throws Exception {
        // kill A general
        setOutput();
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Chess.killEnermy(chess.getPosition());

        assertEquals("Player 1 Lose.\nPlayer 2 Win.", getOutput().trim());
    }

    @Test
    public void chess_killEnermy_4() throws Exception {
        // kill B general
        setOutput();
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
        Chess.killEnermy(chess.getPosition());

        assertEquals("Player 1 Win.\nPlayer 2 Lose.", getOutput().trim());
    }

    // Solider
    @Test
    public void solider_getPossibleMove_1() {
        Solider solider = new Solider(Constants.PLAYER_A);
        solider.setPosition(new Position(2, 9));

        ArrayList<Position> avaPos = solider.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(1, 9));
        expected.add(new Position(3, 9));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void solider_getPossibleMove_2() {
        Solider solider = new Solider(Constants.PLAYER_B);
        solider.setPosition(new Position(2, 0));

        ArrayList<Position> avaPos = solider.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(1, 0));
        expected.add(new Position(3, 0));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void solider_getPossibleMove_3() {
        Solider solider = new Solider(Constants.PLAYER_A);
        solider.setPosition(new Position(3, 3));

        ArrayList<Position> avaPos = solider.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(3, 4));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void solider_getPossibleMove_4() {
        Solider solider = new Solider(Constants.PLAYER_B);
        solider.setPosition(new Position(3, 6));

        ArrayList<Position> avaPos = solider.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(3, 5));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void solider_validMovement_1() {
        // Solider located at 4 4 want to go 10 4, an overflow pos
        Solider chess = new Solider(Constants.PLAYER_A);
        chess.setPosition(new Position(4, 4));
        Position dest = new Position(10, 4);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void solider_validMovement_2() {
        // Solider located at 4 4 move to the same location
        Solider chess = new Solider(Constants.PLAYER_A);
        chess.setPosition(new Position(4, 4));
        Position dest = new Position(4, 4);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void solider_validMovement_3() {
        // Solider located at 4 4 move to invalid position
        Solider chess = new Solider(Constants.PLAYER_A);
        chess.setPosition(new Position(4, 4));
        Position dest = new Position(6, 4);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void solider_validMovement_4() {
        Solider chess = new Solider(Constants.PLAYER_A);
        chess.setPosition(new Position(4, 4));
        Position dest = new Position(4, 5);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void solider_canKillGeneral_1() {
        // can kill general for player A
        Main.createGameObject();
        Main.initGame();
        Solider chess = new Solider(Constants.PLAYER_A);
        chess.setPosition(new Position(4, 8));

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void solider_canKillGeneral_2() {
        // can kill general for player B
        Main.createGameObject();
        Main.initGame();
        Solider chess = new Solider(Constants.PLAYER_B);
        chess.setPosition(new Position(4, 1));

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void solider_canKillGeneral_3() {
        // cannot kill general for player B
        Main.createGameObject();
        Main.initGame();
        Solider chess = new Solider(Constants.PLAYER_B);
        chess.setPosition(new Position(4, 4));

        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void solider_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        Position dest = new Position(4, 4);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void solider_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        Position dest = new Position(0, 4);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void solider_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        Position dest = new Position(0, 5);

        assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
    }

    @Test
    public void solider_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 4);
        Position dest = new Position(0, 4);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void solider_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(0, 4);
        enemy.die();
        Position dest = new Position(0, 4);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void solider_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 4);
        Position dest = new Position(0, 4);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // Scholar
    @Test
    public void scholar_getPossibleMove_1() {
        // Test scholar with pos 3 0
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(3, 0));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_2() {
        // Test scholar with pos 5 0
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(5, 0));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_3() {
        // Test scholar with pos 5 2
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(5, 2));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_4() {
        // Test scholar with pos 3 7
        Scholar scholar1 = new Scholar(Constants.PLAYER_B);
        scholar1.setPosition(new Position(3, 7));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 8));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_validMovement_1() {
        // outside board
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(-1, -1);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_2() {
        // test cross river
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 7);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_3() {
        // test same loc
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 0);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_4() {
        // test valid case
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(4, 1);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_5() {
        // test invalid case
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 1);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void scholar_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 4);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 5);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(4, 1);
        enemy.die();
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // Horse Test
    @Test
    public void horse_possibleMove_1() {
        // normal case for horse pos 1 0
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 0);

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 2));
        expected.add(new Position(2, 2));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_2() {
        // case for horse pos 1 0 and kick foot with pos 1 1
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 0);
        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        chess.setPosition(1, 1);

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_3() {
        // case for horse pos 1 0 and kick foot with pos 1 1
        // and dead chess at 11
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 0);
        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        chess.setPosition(1, 1);
        chess.die();

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 2));
        expected.add(new Position(2, 2));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_4() {
        // normal case for horse pos 1 9
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 9);

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 7));
        expected.add(new Position(2, 7));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_5() {
        // case for horse pos 1 9 with kick foot 1 8
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 9);
        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
        chess.setPosition(1, 8);

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_6() {
        // case for horse pos 1 9 with kick foot 1 8
        // dead chess at 1 8

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(1, 9);
        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
        chess.setPosition(1, 8);
        chess.die();

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(0, 7));
        expected.add(new Position(2, 7));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_7() {
        // case for horse pos 4 8
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(4, 8);

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(2, 7));
        expected.add(new Position(2, 9));
        expected.add(new Position(3, 6));
        expected.add(new Position(5, 6));
        expected.add(new Position(6, 7));
        expected.add(new Position(6, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_8() {
        // case for horse pos 4 8 with kick foot 5 8
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(4, 8);
        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
        chess.setPosition(5, 8);
        chess.die();

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(2, 7));
        expected.add(new Position(2, 9));
        expected.add(new Position(3, 6));
        expected.add(new Position(5, 6));
        expected.add(new Position(6, 7));
        expected.add(new Position(6, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_possibleMove_9() {
        // case for horse pos 5 8 with kick foot 4 8
        // dead kick foot
        Main.createGameObject();
        Main.initGame();

        Horse horse = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        horse.setPosition(5, 8);
        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1];
        chess.setPosition(4, 8);
        chess.die();

        ArrayList<Position> avaList = horse.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(3, 7));
        expected.add(new Position(3, 9));
        expected.add(new Position(7, 7));
        expected.add(new Position(7, 9));
        expected.add(new Position(4, 6));
        expected.add(new Position(6, 6));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaList);

        assertEquals(expected.toString(), avaList.toString());
    }

    @Test
    public void horse_validMovement_1() {
        // overflow pos
        Horse chess = new Horse(Constants.PLAYER_A);
        chess.setPosition(1, 0);

        Position dest = new Position(-1, -1);
        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void horse_validMovement_2() {
        // same pos
        Horse chess = new Horse(Constants.PLAYER_A);
        chess.setPosition(1, 0);

        Position dest = new Position(1, 0);
        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void horse_validMovement_3() {
        // valid move
        Horse chess = new Horse(Constants.PLAYER_A);
        chess.setPosition(1, 0);

        Position dest = new Position(0, 2);
        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void horse_validMovement_4() {
        // invalid move with same X
        Horse chess = new Horse(Constants.PLAYER_A);
        chess.setPosition(1, 0);

        Position dest = new Position(1, 1);
        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void horse_validMovement_5() {
        // invalid move with same Y
        Horse chess = new Horse(Constants.PLAYER_A);
        chess.setPosition(1, 0);

        Position dest = new Position(2, 0);
        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void horse_canKillGeneral_1() {
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        chess.setPosition(3, 7);

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void horse_canKillGeneral_2() {
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        chess.setPosition(3, 8);

        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void horse_canKillGeneral_3() {
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1];
        chess.setPosition(3, 2);

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void horse_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 4);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void horse_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void horse_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        Position dest = new Position(4, 5);

        assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
    }

    @Test
    public void horse_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void horse_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(0, 2);
        enemy.die();
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void horse_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Horse chess = (Horse) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
        Position dest = new Position(0, 2);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // General
    @Test
    public void general_getPossibleMove_1() {
        // Test for general for A side
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 1));
        expectedResult.add(new Position(3, 0));
        expectedResult.add(new Position(5, 0));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_2() {
        // Test for general, the x pos of A and B general are different
        Main.createGameObject();
        Main.initGame();

        General generalA = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        General generalB = (General) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
        generalA.setPosition(3, 0);
        generalB.setPosition(4, 9);
        ArrayList<Position> avaPos = generalB.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(3, 9));
        expectedResult.add(new Position(4, 8));
        expectedResult.add(new Position(5, 9));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_3() {
        // Test for general, pos 5 0
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        chess.setPosition(5, 0);
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 0));
        expectedResult.add(new Position(5, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_4() {
        // Test for general A, pos 3 0
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        chess.setPosition(3, 0);
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 0));
        expectedResult.add(new Position(3, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_5() {
        // Test for general A, pos 4 2
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        chess.setPosition(4, 2);
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(3, 2));
        expectedResult.add(new Position(4, 1));
        expectedResult.add(new Position(5, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_6() {
        // Test for general B, pos 4 7
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
        chess.setPosition(4, 7);
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 8));
        expectedResult.add(new Position(3, 7));
        expectedResult.add(new Position(5, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_7() {
        // Test for general A 飛將
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
        Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];

        solider_A_3.setPosition(5, 4);
        solider_B_3.setPosition(5, 6);
        chess.setPosition(4, 0);

        System.out.println(chess.getOwner());

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 1));
        expectedResult.add(new Position(4, 9));
        expectedResult.add(new Position(3, 0));
        expectedResult.add(new Position(5, 0));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_getPossibleMove_9() {
        // Test for general B 飛將
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
        Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
        Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];

        solider_A_3.setPosition(5, 4);
        solider_B_3.setPosition(5, 6);
        chess.setPosition(4, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expectedResult = new ArrayList<Position>();
        expectedResult.add(new Position(4, 0));
        expectedResult.add(new Position(4, 8));
        expectedResult.add(new Position(3, 9));
        expectedResult.add(new Position(5, 9));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expectedResult);

        assertEquals(expectedResult.toString(), avaPos.toString());
    }

    @Test
    public void general_validMovement_1() {
        // test outbound pos
        General chess = new General(Constants.PLAYER_A);
        chess.setPosition(4, 0);
        Position dest = new Position(-1, -1);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void general_validMovement_2() {
        // test same pos
        General chess = new General(Constants.PLAYER_A);
        chess.setPosition(4, 0);
        Position dest = new Position(4, 0);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void general_validMovement_3() {
        // valid case
        Main.createGameObject();
        Main.initGame();

        General chess = new General(Constants.PLAYER_A);
        chess.setPosition(4, 0);
        Position dest = new Position(4, 1);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void general_validMovement_4() {
        // invalid position case
        Main.createGameObject();
        Main.initGame();

        General chess = new General(Constants.PLAYER_A);
        chess.setPosition(4, 0);
        Position dest = new Position(5, 1);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void general_canKillGeneral_1() {
        // cant kill general for A
        Main.createGameObject();
        Main.initGame();

        General chess = new General(Constants.PLAYER_A);
        chess.setPosition(4, 0);
        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void general_canKillGeneral_2() {
        // cant kill general for B
        Main.createGameObject();
        Main.initGame();

        General chess = new General(Constants.PLAYER_B);
        chess.setPosition(4, 9);
        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void general_canKillGeneral_3() {
        // can kill general by fly
        General chess = (General) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
        Chess solider_A_3 = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3];
        Chess solider_B_3 = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3];

        solider_A_3.setPosition(5, 4);
        solider_B_3.setPosition(5, 6);
        chess.setPosition(4, 9);

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void general_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Position dest = new Position(4, 0);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void general_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void general_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Position dest = new Position(4, 5);

        assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
    }

    @Test
    public void general_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void general_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(4, 1);
        enemy.die();
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void general_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        General chess = (General) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // Elephant
    @Test
    public void elephant_getPossibleMovement_1() {
        // Valid case with pos 2 0, no kick foot
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(0, 2));
        expected.add(new Position(4, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_2() {
        // elephant pos 2 0, no kick foot
        // for the checkkickfootresult that +1 +1
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        chess.setPosition(2, 0);
        Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        kickFoot.setPosition(3, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(0, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_3() {
        // elephant with pos 2 0, with dead kick foot
        // for the checkkickfootresult that +1 +1
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        chess.setPosition(2, 0);
        Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        kickFoot.setPosition(3, 1);
        kickFoot.die();

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(0, 2));
        expected.add(new Position(4, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_4() {
        // valid case, no kick foot case, pos 6 9
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        chess.setPosition(6, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 7));
        expected.add(new Position(8, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }


    @Test
    public void elephant_getPossibleMovement_5() throws Exception {
        // with kick foot case
        // Test elephant pos 6 9 , with kick foot chess with pos 7 8
        Main.createGameObject();
        Main.initGame();

        Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        kickFoot.setPosition(7, 8);
        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        chess.setPosition(6, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_6() {
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        chess.setPosition(6, 9);
        Chess kickFoot = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
        kickFoot.setPosition(7, 8);
        kickFoot.die();

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 7));
        expected.add(new Position(8, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_7() {
        // Test for elephant with pos 6 0, with kickFoot 5 1 pos
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_2];
        chess.setPosition(6, 0);

        Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        kickFoot.setPosition(5, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(8, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_8() {
        // Test for elephant with pos 6 0, with kickFoot 5 1 pos(dead chess)
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_2];
        chess.setPosition(6, 0);

        Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        kickFoot.setPosition(5, 1);
        kickFoot.die();

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(8, 2));
        expected.add(new Position(4, 2));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_9() {
        // Test for elephant with pos 2 9, with kickFoot 1 8 pos
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_1];
        chess.setPosition(2, 9);

        Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        kickFoot.setPosition(1, 8);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_getPossibleMovement_10() {
        // Test for elephant with pos 6 0, with kickFoot 5 1 pos(dead chess)
        Main.createGameObject();
        Main.initGame();

        Chess chess = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_1];
        chess.setPosition(2, 9);

        Chess kickFoot = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2];
        kickFoot.setPosition(1, 8);
        kickFoot.die();

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 7));
        expected.add(new Position(0, 7));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void elephant_validMovement_1() {
        // test overflow pos
        Main.createGameObject();
        Main.initGame();
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);
        Position dest = new Position(-1, -1);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void elephant_validMovement_2() {
        // test cross river
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);
        Position dest = new Position(0, 7);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.validMovement(dest));
    }

    @Test
    public void elephant_validMovement_3() {
        // test same pos
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);
        Position dest = new Position(2, 0);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void elephant_validMovement_4() {
        // test invalid move
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);
        Position dest = new Position(0, 3);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void elephant_validMovement_5() {
        // test valid move
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);

        Position dest = new Position(4, 2);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void elephant_validMovement_6() {
        // test invalid move, same X
        Elephant chess = new Elephant(Constants.PLAYER_A);
        chess.setPosition(2, 0);
        Position dest = new Position(2, 1);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void elephant_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        Position dest = new Position(4, 0);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void elephant_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void elephant_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        Position dest = new Position(4, 5);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.goTo(dest));
    }

    @Test
    public void elephant_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 2);
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void elephant_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(0, 2);
        enemy.die();
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void elephant_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Elephant chess = (Elephant) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
        Position dest = new Position(0, 2);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // Chariot
    @Test
    public void chariot_getPossible_1() {
        // chariot pos 0 0
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        chess.setPosition(0, 0);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(1, 0));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 3));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void chariot_getPossible_2() {
        // chariot pos 8 0
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_2];
        chess.setPosition(8, 0);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 1));
        expected.add(new Position(7, 0));
        expected.add(new Position(8, 2));
        expected.add(new Position(8, 3));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void chariot_getPossible_3() {
        // chariot pos 0 9
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_1];
        chess.setPosition(0, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 6));
        expected.add(new Position(1, 9));
        expected.add(new Position(0, 7));
        expected.add(new Position(0, 8));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void chariot_getPossible_4() {
        // chariot pos 8 8
        // Y axis 8 has B chariot_2 only
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2];
        chess.setPosition(8, 8);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 8));
        expected.add(new Position(1, 8));
        expected.add(new Position(2, 8));
        expected.add(new Position(3, 8));
        expected.add(new Position(4, 8));
        expected.add(new Position(5, 8));
        expected.add(new Position(6, 8));
        expected.add(new Position(7, 8));

        expected.add(new Position(8, 6));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void chariot_getPossible_5() {
        // chariot pos 0 1
        // Y axis 1 has A chariot_1 only
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        chess.setPosition(0, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));
        expected.add(new Position(8, 1));

        expected.add(new Position(0, 0));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 3));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void chariot_validMovement_1() {
        // pos overflow -1 -1
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(0, 0);
        Position dest = new Position(-1, -1);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void chariot_validMovement_2() {
        // pos same
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(0, 0);
        Position dest = new Position(0, 0);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void chariot_validMovement_3() {
        // pos overflow -1 -1
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(0, 0);
        Position dest = new Position(0, 1);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void chariot_validMovement_4() {
        // pos invalid
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(0, 0);
        Position dest = new Position(1, 1);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void chariot_canKillGeneral_1() {
        // pos can kill B general
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(4, 7);

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void chariot_canKillGeneral_2() {
        // pos cannot kill B general
        Chariot chess = new Chariot(Constants.PLAYER_A);
        chess.setPosition(0, 0);

        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void chariot_canKillGeneral_3() {
        // pos cannot kill A general
        Chariot chess = new Chariot(Constants.PLAYER_B);
        chess.setPosition(0, 0);

        assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void chariot_canKillGeneral_4() {
        // pos can kill A general
        Chariot chess = new Chariot(Constants.PLAYER_B);
        chess.setPosition(4, 1);

        assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
    }

    @Test
    public void chariot_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        Position dest = new Position(4, 0);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void chariot_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void chariot_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        Position dest = new Position(4, 5);

        assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
    }

    @Test
    public void chariot_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 2);
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void chariot_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(0, 2);
        enemy.die();
        Position dest = new Position(0, 2);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void chariot_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Chariot chess = (Chariot) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 2);
        Position dest = new Position(0, 2);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }

    // Cannon
    @Test
    public void cannon_getPossibleMove_1() {
        // pos 0 9 A cannon
        // all chess exist in y asix 9
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(2, 9));
        expected.add(new Position(0, 8));
        expected.add(new Position(0, 7));
        expected.add(new Position(0, 3));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_2() {
        // pos 0 1
        // no chess in the y asix 1
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));

        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));
        expected.add(new Position(8, 1));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_3() {
        // pos 0 0
        // all chesses exist in the y asix 0
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 0);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));
        expected.add(new Position(2, 0));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_4() {
        // pos 8 9 B cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 8));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 3));
        expected.add(new Position(6, 9));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_5() {
        // pos 8 9 A cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 8));
        expected.add(new Position(8, 7));
        expected.add(new Position(6, 9));
        expected.add(new Position(8, 3));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_6() {
        // pos
        // X axis 8 has no chess except cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 0);

        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(7, 3);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_5].setPosition(7, 6);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2].setPosition(7, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 1));
        expected.add(new Position(8, 2));
        expected.add(new Position(8, 3));
        expected.add(new Position(8, 4));
        expected.add(new Position(8, 5));
        expected.add(new Position(8, 6));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 8));
        expected.add(new Position(8, 9));

        expected.add(new Position(6, 0));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_7() {
        // pos 8 0
        // X axis 8 all chess
        // Y axis 0 all chess
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 0);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(8, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(8, 2);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_4].setPosition(8, 3);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(8, 4);

        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2].setPosition(8, 5);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3].setPosition(8, 6);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_4].setPosition(8, 7);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_5].setPosition(8, 8);

        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2].setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(6, 0));
        expected.add(new Position(8, 2));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_8() {
        // pos
        // Y axis 1 has no chess except itselfs
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 6));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_9() {
        Main.createGameObject();
        Main.initGame();

        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(1, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_4].setPosition(5, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(3, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(2, 1);

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);


        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(7, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(8, 2));
        expected.add(new Position(8, 3));
        expected.add(new Position(8, 4));
        expected.add(new Position(8, 5));
        expected.add(new Position(8, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_10() {
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 8));
        expected.add(new Position(0, 7));
        expected.add(new Position(0, 3));
        expected.add(new Position(2, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_11() {
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 0);
        
        //move horse 1 and elephant 1
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1].setPosition(4,4);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1].setPosition(4,5);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(1, 0));
        expected.add(new Position(4, 0));
        expected.add(new Position(0, 1));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_12() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2].setPosition(8, 4);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3].setPosition(8, 5);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 5));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 8));

        expected.add(new Position(6, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_13() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(8, 4);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(8, 5);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 4));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }
    
    @Test
    public void cannon_getPossibleMove_14() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(3, 1);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 6));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_validMovement_1() {
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(-1, -1);
        assertEquals(Constants.POSITION_OUT_OF_BOUND, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_2() {
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 0);
        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_3() {
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(1, 0);
        assertEquals(Constants.POSITION_INVALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_4() {
        // y asix move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 6);
        assertEquals(Constants.MOVEMENT_VALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_5() {
        // x asix move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(2, 0);
        assertEquals(Constants.MOVEMENT_VALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_canKillGeneral_1() {
        // Player A kill Player B
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(4, 5);

        assertEquals(Constants.I_CAN_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_canKillGeneral_2() {
        // Player B kill Player A
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_B);
        cannon.setPosition(4, 5);

        assertEquals(Constants.I_CAN_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_canKillGeneral_3() {
        // cannot kill general
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        assertEquals(Constants.I_CANT_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_goTo_1() {
        // move dead chess
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);
        cannon.die();

        Position dest = new Position(0, 0);
        assertEquals(Constants.CHESS_IS_NOT_ALIVE, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_2() {
        // move to colleague pos
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(2, 0);
        assertEquals(Constants.POSITION_HAS_OWN_CHESS, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_3() {
        // move valid - eat chess
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 6);
        assertEquals(Constants.CHESS_MOVED_SUCCESS, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_4() {
        // move invalid - overflow pos
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(-1, -1);
        assertEquals(Constants.POSITION_OUT_OF_BOUND, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_5() {
        // move valid - just move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 1);
        assertEquals(Constants.CHESS_MOVED_SUCCESS, cannon.goTo(dest));
    }
    
    // Commandhandler
    @Test
    public void commandhandler_receiveCommand_1() {
        // Test len != 4 and len != 2
        int result = CommandHandler.receiveCommand("1 2 3");
        assertEquals(Constants.COMMAND_PARAM_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_2() {
        // Test len != 4 and len == 2 and negative number
        int result = CommandHandler.receiveCommand("-1 2");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_3() {
        // Test len != 4 and len == 2 and overflow X index
        int result = CommandHandler.receiveCommand("99 2");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_4() {
        // Test len != 4 and len == 2 and overflow Y index
        int result = CommandHandler.receiveCommand("2 19");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_5() {
        // Test len != 4 and len == 2 and not int
        int result = CommandHandler.receiveCommand("a b");
        assertEquals(Constants.COMMAND_NOT_INTEGER, result);
    }

    @Test
    public void commandhandler_receiveCommand_7() {
        Main.createGameObject();
        Main.initGame();

        // Test len == 2 and pos provided have no chess
        int result = CommandHandler.receiveCommand("0 1");
        assertEquals(Constants.NO_THIS_CHESS, result);
    }

    @Test
    public void commandhandler_receiveCommand_8() {
        Main.createGameObject();
        Main.initGame();

        // Test len == 2 and pos provided have chess
        int result = CommandHandler.receiveCommand("0 0");
        assertEquals(Constants.COMMAND_PARAM_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_9() {
        // Test len == 4 and pos negative
        int result = CommandHandler.receiveCommand("-1 0 0 0");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_10() {
        // Test len == 4 and X pos overflow
        int result = CommandHandler.receiveCommand("99 0 0 0");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_11() {
        // Test len == 4 and Y pos overflow
        int result = CommandHandler.receiveCommand("0 99 0 0");
        assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
    }

    @Test
    public void commandhandler_receiveCommand_12() {
        // Test len == 4 and not int
        int result = CommandHandler.receiveCommand("a a a a");
        assertEquals(Constants.COMMAND_NOT_INTEGER, result);
    }

    @Test
    public void commandhandler_receiveCommand_13() {
        Main.createGameObject();
        Main.initGame();

        // Test len == 4 and no chess for 2 pos
        int result = CommandHandler.receiveCommand("0 1 1 1");
        assertEquals(Constants.NO_THIS_CHESS, result);
    }

    @Test
    public void commandhandler_receiveCommand_14() {
        Main.createGameObject();
        Main.initGame();
        GameData.currentTurn = Constants.PLAYER_A;
        // Test len == 4 and Player A turn control player B chess
        int result = CommandHandler.receiveCommand("0 9 0 8");
        assertEquals(Constants.NOT_CHESS_OWNER, result);
    }

    @Test
    public void commandhandler_receiveCommand_15() {
        Main.createGameObject();
        Main.initGame();
        GameData.currentTurn = Constants.PLAYER_B;
        // Test len == 4 and Player B turn control player A chess
        int result = CommandHandler.receiveCommand("0 0 0 1");
        assertEquals(Constants.NOT_CHESS_OWNER, result);
    }

    @Test
    public void commandhandler_receiveCommand_16() {
        Main.createGameObject();
        Main.initGame();
        GameData.currentTurn = Constants.PLAYER_A;
        // Test len == 4 and player A turn with valid move
        int result = CommandHandler.receiveCommand("0 0 0 1");
        assertEquals(Constants.COMMAND_SUCCESS, result);
    }

    @Test
    public void commandhandler_receiveCommand_17() {
        Main.createGameObject();
        Main.initGame();
        GameData.currentTurn = Constants.PLAYER_B;// unknown player
        // Test len == 4 and player B turn with valid move
        int result = CommandHandler.receiveCommand("0 9 0 8");
        assertEquals(Constants.COMMAND_SUCCESS, result);
    }

    @Test
    public void commandhandler_receiveCommand_18() {
        Main.createGameObject();
        Main.initGame();
        GameData.currentTurn = Constants.PLAYER_B;// unknown player
        // Test len == 4 and player A turn
        // move to colleage
        int result = CommandHandler.receiveCommand("0 9 1 9");
        assertEquals(Constants.POSITION_HAS_OWN_CHESS, result);
    }

}
