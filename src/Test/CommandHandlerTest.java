package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import GlobalData.Constants;
import GlobalData.GameData;
import Run.CommandHandler;
import Run.Main;

public class CommandHandlerTest {

	//after all chess type
	@Test
	public void commandhandler_receiveCommand_1(){
		//Test len != 4 and len != 2
		int result = CommandHandler.receiveCommand("1 2 3");
		assertEquals(Constants.COMMAND_PARAM_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_2(){
		//Test len != 4 and len == 2 and negative number
		int result = CommandHandler.receiveCommand("-1 2");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_3(){
		//Test len != 4 and len == 2 and overflow X index
		int result = CommandHandler.receiveCommand("99 2");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_4(){
		//Test len != 4 and len == 2 and overflow Y index
		int result = CommandHandler.receiveCommand("2 19");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_5(){
		//Test len != 4 and len == 2 and not int
		int result = CommandHandler.receiveCommand("a b");
		assertEquals(Constants.COMMAND_NOT_INTEGER, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_7(){
		Main.createGameObject();
		Main.initGame();
		
		//Test len == 2 and pos provided have no chess
		int result = CommandHandler.receiveCommand("0 1");
		assertEquals(Constants.NO_THIS_CHESS, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_8(){
		Main.createGameObject();
		Main.initGame();
		
		//Test len == 2 and pos provided have chess
		int result = CommandHandler.receiveCommand("0 0");
		assertEquals(Constants.COMMAND_PARAM_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_9(){
		//Test len == 4 and pos negative
		int result = CommandHandler.receiveCommand("-1 0 0 0");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_10(){
		//Test len == 4 and X pos overflow
		int result = CommandHandler.receiveCommand("99 0 0 0");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_11(){
		//Test len == 4 and Y pos overflow
		int result = CommandHandler.receiveCommand("0 99 0 0");
		assertEquals(Constants.COMMAND_INTEGER_INVALID, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_12(){
		//Test len == 4 and not int
		int result = CommandHandler.receiveCommand("a a a a");
		assertEquals(Constants.COMMAND_NOT_INTEGER, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_13(){
		Main.createGameObject();
		Main.initGame();
		
		//Test len == 4 and no chess for 2 pos
		int result = CommandHandler.receiveCommand("0 1 1 1");
		assertEquals(Constants.NO_THIS_CHESS, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_14(){
		Main.createGameObject();
		Main.initGame();
		GameData.currentTurn = Constants.PLAYER_A;
		//Test len == 4 and Player A turn control player B chess
		int result = CommandHandler.receiveCommand("0 9 0 8");
		assertEquals(Constants.NOT_CHESS_OWNER, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_15(){
		Main.createGameObject();
		Main.initGame();
		GameData.currentTurn = Constants.PLAYER_B;
		//Test len == 4 and Player B turn control player A chess
		int result = CommandHandler.receiveCommand("0 0 0 1");
		assertEquals(Constants.NOT_CHESS_OWNER, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_16(){
		Main.createGameObject();
		Main.initGame();
		GameData.currentTurn = Constants.PLAYER_A;
		//Test len == 4 and player A turn with valid move
		int result = CommandHandler.receiveCommand("0 0 0 1");
		assertEquals(Constants.COMMAND_SUCCESS, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_17(){
		Main.createGameObject();
		Main.initGame();
		GameData.currentTurn = Constants.PLAYER_B;// unknown player
		//Test len == 4 and player B turn with valid move
		int result = CommandHandler.receiveCommand("0 9 0 8");
		assertEquals(Constants.COMMAND_SUCCESS, result);
	}
	
	@Test
	public void commandhandler_receiveCommand_18(){
		Main.createGameObject();
		Main.initGame();
		GameData.currentTurn = Constants.PLAYER_B;// unknown player
		//Test len == 4 and player A turn 
		//move to colleage
		int result = CommandHandler.receiveCommand("0 9 1 9");
		assertEquals(Constants.POSITION_HAS_OWN_CHESS, result);
	}

}
