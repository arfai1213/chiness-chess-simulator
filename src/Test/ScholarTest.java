package Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Position;
import ChessType.Scholar;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class ScholarTest {

    // Start test for Scholar
    @Test
    public void scholar_getPossibleMove_1() {
        // Test scholar with pos 3 0
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(3, 0));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_2() {
        // Test scholar with pos 5 0
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(5, 0));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_3() {
        // Test scholar with pos 5 2
        Scholar scholar1 = new Scholar(Constants.PLAYER_A);
        scholar1.setPosition(new Position(5, 2));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 1));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_getPossibleMove_4() {
        // Test scholar with pos 3 7
        Scholar scholar1 = new Scholar(Constants.PLAYER_B);
        scholar1.setPosition(new Position(3, 7));

        ArrayList<Position> avaPos = scholar1.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();
        expected.add(new Position(4, 8));

        TestUtil.sortPosition(avaPos);
        TestUtil.sortPosition(expected);

        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void scholar_validMovement_1() {
        // outside board
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(-1, -1);

        assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_2() {
        // test cross river
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 7);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_3() {
        // test same loc
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 0);

        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_4() {
        // test valid case
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(4, 1);

        assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
    }

    @Test
    public void scholar_validMovement_5() {
        // test invalid case
        Scholar chess = new Scholar(Constants.PLAYER_A);
        chess.setPosition(new Position(3, 0));
        Position dest = new Position(3, 1);

        assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
    }

    @Test
    public void scholar_goTo_1() {
        // dead chess move
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 4);
        chess.die();

        assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_2() {
        // valid move
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_3() {
        // invalid move, cross river
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Position dest = new Position(4, 5);

        assertEquals(Constants.SHOULD_NOT_CROSS_RIVER, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_4() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_5() {
        // go to dead chess pos
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        Solider enemy = (Solider) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
        enemy.setPosition(4, 1);
        enemy.die();
        Position dest = new Position(4, 1);

        assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
    }

    @Test
    public void scholar_goTo_6() {
        // eat chess
        Main.createGameObject();
        Main.initGame();

        Scholar chess = (Scholar) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1];
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(4, 1);
        Position dest = new Position(4, 1);

        assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
    }
}
