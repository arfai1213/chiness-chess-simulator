package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Position;
import ChessType.Solider;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class SoliderTest {

	//Start test for Solider
		@Test
		public void solider_getPossibleMove_1() {		
			Solider solider = new Solider(Constants.PLAYER_A);
			solider.setPosition(new Position(2,9));
			
			ArrayList<Position> avaPos = solider.getPossibleMove();
			ArrayList<Position> expected = new ArrayList<Position>();
			expected.add(new Position(1,9));
			expected.add(new Position(3,9));

			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
			
			assertEquals(expected.toString(), avaPos.toString());
		}
		
		@Test
		public void solider_getPossibleMove_2(){
			Solider solider = new Solider(Constants.PLAYER_B);
			solider.setPosition(new Position(2,0));
			
			ArrayList<Position> avaPos = solider.getPossibleMove();
			ArrayList<Position> expected = new ArrayList<Position>();
			expected.add(new Position(1,0));
			expected.add(new Position(3,0));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
			
			assertEquals(expected.toString(), avaPos.toString());
		}
		
		@Test
		public void solider_getPossibleMove_3(){
			Solider solider = new Solider(Constants.PLAYER_A);
			solider.setPosition(new Position(3,3));
			
			ArrayList<Position> avaPos = solider.getPossibleMove();
			ArrayList<Position> expected = new ArrayList<Position>();
			expected.add(new Position(3,4));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
			
			assertEquals(expected.toString(), avaPos.toString());
		}
		
		@Test
		public void solider_getPossibleMove_4(){
			Solider solider = new Solider(Constants.PLAYER_B);
			solider.setPosition(new Position(3,6));
			
			ArrayList<Position> avaPos = solider.getPossibleMove();
			ArrayList<Position> expected = new ArrayList<Position>();
			expected.add(new Position(3,5));
			
			TestUtil.sortPosition(avaPos);TestUtil.sortPosition(expected);
			
			assertEquals(expected.toString(), avaPos.toString());
		}
		
		@Test
		public void solider_validMovement_1(){
			//Solider located at 4 4 want to go 10 4, an overflow pos
			Solider chess = new Solider(Constants.PLAYER_A);
			chess.setPosition(new Position(4,4));
			Position dest = new Position(10,4);
			
			assertEquals(Constants.POSITION_OUT_OF_BOUND, chess.validMovement(dest));
		}
		
		@Test
		public void solider_validMovement_2(){
			//Solider located at 4 4 move to the same location
			Solider chess = new Solider(Constants.PLAYER_A);
			chess.setPosition(new Position(4,4));
			Position dest = new Position(4,4);
			
			assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, chess.validMovement(dest));
		}
		
		@Test
		public void solider_validMovement_3(){
			//Solider located at 4 4 move to invalid position
			Solider chess = new Solider(Constants.PLAYER_A);
			chess.setPosition(new Position(4,4));
			Position dest = new Position(6,4);
			
			assertEquals(Constants.POSITION_INVALID, chess.validMovement(dest));
		}
		
		@Test
		public void solider_validMovement_4(){
			Solider chess = new Solider(Constants.PLAYER_A);
			chess.setPosition(new Position(4,4));
			Position dest = new Position(4,5);
			
			assertEquals(Constants.MOVEMENT_VALID, chess.validMovement(dest));
		}
		
		@Test
		public void solider_canKillGeneral_1(){
			//can kill general for player A
			Main.createGameObject();
			Main.initGame();
			Solider chess = new Solider(Constants.PLAYER_A);
			chess.setPosition(new Position(4,8));
			
			assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void solider_canKillGeneral_2(){
			//can kill general for player B
			Main.createGameObject();
			Main.initGame();
			Solider chess = new Solider(Constants.PLAYER_B);
			chess.setPosition(new Position(4,1));
			
			assertEquals(Constants.I_CAN_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void solider_canKillGeneral_3(){
			//cannot kill general for player B
			Main.createGameObject();
			Main.initGame();
			Solider chess = new Solider(Constants.PLAYER_B);
			chess.setPosition(new Position(4,4));
			
			assertEquals(Constants.I_CANT_KILL_YOU, chess.canKillGeneral());
		}
		
		@Test
		public void solider_goTo_1(){
			//dead chess move
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			Position dest = new Position(4,4);
			chess.die();
			
			assertEquals(Constants.CHESS_IS_NOT_ALIVE, chess.goTo(dest));
		}
		
		@Test
		public void solider_goTo_2(){
			//valid move
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			Position dest = new Position(0,4);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void solider_goTo_3(){
			//invalid move, cross river
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			Position dest = new Position(0,5);
			
			assertEquals(Constants.POSITION_INVALID, chess.goTo(dest));
		}
		
		@Test
		public void solider_goTo_4(){
			//eat chess
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 4);
			Position dest = new Position(0,4);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void solider_goTo_5(){
			//go to dead chess pos
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			Solider enemy = (Solider)GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2];
			enemy.setPosition(0, 4);
			enemy.die();
			Position dest = new Position(0,4);
			
			assertEquals(Constants.CHESS_MOVED_SUCCESS, chess.goTo(dest));
		}
		
		@Test
		public void solider_goTo_6(){
			//eat chess
			Main.createGameObject();
			Main.initGame();
			
			Solider chess = (Solider) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1];
			GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(0, 4);
			Position dest = new Position(0,4);
			
			assertEquals(Constants.POSITION_HAS_OWN_CHESS, chess.goTo(dest));
		}

}
