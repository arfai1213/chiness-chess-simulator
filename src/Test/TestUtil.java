package Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ChessType.Position;

public class TestUtil {
	public static List<Position> sortPosition(List<Position> allPos){
		Collections.sort(allPos, new Comparator<Position>(){
			@Override
	        public int compare(Position  pos1, Position  pos2)
	        {
				if(pos1.getX() == pos2.getX() && pos1.getY() == pos2.getY())
					return 0;
				if(pos1.getX() > pos2.getX())
					return 1;
				else if(pos1.getX() == pos2.getX()){
					if(pos1.getY() > pos2.getY())
						return 1;
					else 
						return -1;
				}else{
					return -1;
				}
	        }
		});
		return allPos;
	}
}
