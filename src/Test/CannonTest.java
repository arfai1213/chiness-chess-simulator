package Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Cannon;
import ChessType.Position;
import GlobalData.Constants;
import GlobalData.GameData;
import Run.Main;

public class CannonTest {

    @Test
    public void cannon_getPossibleMove_1() {
        // pos 0 9 A cannon
        // all chess exist in y asix 9
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(2, 9));
        expected.add(new Position(0, 8));
        expected.add(new Position(0, 7));
        expected.add(new Position(0, 3));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_2() {
        // pos 0 1
        // no chess in the y asix 1
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));

        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));
        expected.add(new Position(8, 1));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_3() {
        // pos 0 0
        // all chesses exist in the y asix 0
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 0);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));
        expected.add(new Position(2, 0));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_4() {
        // pos 8 9 B cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 8));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 3));
        expected.add(new Position(6, 9));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_5() {
        // pos 8 9 A cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 8));
        expected.add(new Position(8, 7));
        expected.add(new Position(6, 9));
        expected.add(new Position(8, 3));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_6() {
        // pos
        // X axis 8 has no chess except cannon
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 0);

        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(7, 3);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_5].setPosition(7, 6);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2].setPosition(7, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 1));
        expected.add(new Position(8, 2));
        expected.add(new Position(8, 3));
        expected.add(new Position(8, 4));
        expected.add(new Position(8, 5));
        expected.add(new Position(8, 6));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 8));
        expected.add(new Position(8, 9));

        expected.add(new Position(6, 0));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_7() {
        // pos 8 0
        // X axis 8 all chess
        // Y axis 0 all chess
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 0);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(8, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(8, 2);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_4].setPosition(8, 3);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(8, 4);

        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2].setPosition(8, 5);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3].setPosition(8, 6);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_4].setPosition(8, 7);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_5].setPosition(8, 8);

        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2].setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(6, 0));
        expected.add(new Position(8, 2));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_8() {
        // pos
        // Y axis 1 has no chess except itselfs
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 6));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_9() {
        Main.createGameObject();
        Main.initGame();

        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(1, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_4].setPosition(5, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1].setPosition(4, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(3, 1);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(2, 1);

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);


        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(7, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(8, 2));
        expected.add(new Position(8, 3));
        expected.add(new Position(8, 4));
        expected.add(new Position(8, 5));
        expected.add(new Position(8, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_10() {
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 8));
        expected.add(new Position(0, 7));
        expected.add(new Position(0, 3));
        expected.add(new Position(2, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_11() {
        Main.createGameObject();
        Main.initGame();

        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1];
        chess.setPosition(0, 0);
        
        //move horse 1 and elephant 1
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1].setPosition(4,4);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1].setPosition(4,5);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(1, 0));
        expected.add(new Position(4, 0));
        expected.add(new Position(0, 1));
        expected.add(new Position(0, 2));
        expected.add(new Position(0, 6));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_12() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2].setPosition(8, 4);
        GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3].setPosition(8, 5);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        chess.setPosition(8, 9);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(8, 5));
        expected.add(new Position(8, 7));
        expected.add(new Position(8, 8));

        expected.add(new Position(6, 9));

        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_getPossibleMove_13() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(8, 4);
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(8, 5);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(0, 1));
        expected.add(new Position(1, 1));
        expected.add(new Position(2, 1));
        expected.add(new Position(3, 1));
        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 4));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }
    
    @Test
    public void cannon_getPossibleMove_14() {
        Main.createGameObject();
        Main.initGame();
        GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(3, 1);
        Cannon chess = (Cannon) GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2];
        chess.setPosition(8, 1);

        ArrayList<Position> avaPos = chess.getPossibleMove();
        ArrayList<Position> expected = new ArrayList<Position>();

        expected.add(new Position(4, 1));
        expected.add(new Position(5, 1));
        expected.add(new Position(6, 1));
        expected.add(new Position(7, 1));

        expected.add(new Position(8, 2));
        expected.add(new Position(8, 6));


        TestUtil.sortPosition(expected);
        TestUtil.sortPosition(avaPos);
        assertEquals(expected.toString(), avaPos.toString());
    }

    @Test
    public void cannon_validMovement_1() {
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(-1, -1);
        assertEquals(Constants.POSITION_OUT_OF_BOUND, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_2() {
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 0);
        assertEquals(Constants.THIS_IS_ALREADY_YOUR_POSITION, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_3() {
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(1, 0);
        assertEquals(Constants.POSITION_INVALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_4() {
        // y asix move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 6);
        assertEquals(Constants.MOVEMENT_VALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_validMovement_5() {
        // x asix move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        Position dest = new Position(2, 0);
        assertEquals(Constants.MOVEMENT_VALID, cannon.validMovement(dest));
    }

    @Test
    public void cannon_canKillGeneral_1() {
        // Player A kill Player B
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(4, 5);

        assertEquals(Constants.I_CAN_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_canKillGeneral_2() {
        // Player B kill Player A
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_B);
        cannon.setPosition(4, 5);

        assertEquals(Constants.I_CAN_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_canKillGeneral_3() {
        // cannot kill general
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);

        assertEquals(Constants.I_CANT_KILL_YOU, cannon.canKillGeneral());
    }

    @Test
    public void cannon_goTo_1() {
        // move dead chess
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = new Cannon(Constants.PLAYER_A);
        cannon.setPosition(0, 0);
        cannon.die();

        Position dest = new Position(0, 0);
        assertEquals(Constants.CHESS_IS_NOT_ALIVE, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_2() {
        // move to colleague pos
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(2, 0);
        assertEquals(Constants.POSITION_HAS_OWN_CHESS, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_3() {
        // move valid - eat chess
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 6);
        assertEquals(Constants.CHESS_MOVED_SUCCESS, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_4() {
        // move invalid - overflow pos
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(-1, -1);
        assertEquals(Constants.POSITION_OUT_OF_BOUND, cannon.goTo(dest));
    }

    @Test
    public void cannon_goTo_5() {
        // move valid - just move
        Main.createGameObject();
        Main.initGame();
        Cannon cannon = (Cannon) GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1];
        cannon.setPosition(0, 0);

        Position dest = new Position(0, 1);
        assertEquals(Constants.CHESS_MOVED_SUCCESS, cannon.goTo(dest));
    }

}
