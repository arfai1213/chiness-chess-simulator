package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ChessType.Position;
import GlobalData.Constants;

public class PositionTest {
	@Test
	public void position_isInsideBoard_1(){
		//Test valid position
		Position pos = new Position(4,4);
		assertEquals(Constants.POSITION_VALID, Position.isInsideBoard(pos));
	}
	
	@Test
	public void position_isInsideBoard_2(){
		//Test overflow X
		Position pos = new Position(10,4);
		assertEquals(Constants.POSITION_X_INVALID, Position.isInsideBoard(pos));
	}
	
	@Test
	public void position_isInsideBoard_3(){
		//Test negative X
		Position pos = new Position(-1,4);
		assertEquals(Constants.POSITION_X_INVALID, Position.isInsideBoard(pos));
	}
	
	@Test
	public void position_isInsideBoard_4(){
		//Test overflow Y
		Position pos = new Position(4,11);
		assertEquals(Constants.POSITION_Y_INVALID, Position.isInsideBoard(pos));
	}
	
	@Test
	public void position_isInsideBoard_5(){
		//Test negative Y
		Position pos = new Position(4,-1);
		assertEquals(Constants.POSITION_Y_INVALID, Position.isInsideBoard(pos));
	}
	
	@Test
	public void position_isCrossRiver_1(){
		// For A side, Pos 4 4 is not CrossRiver
		Position pos = new Position(4,4);
		assertEquals(false, Position.isCrossRiver(Constants.PLAYER_A, pos));
	}
	
	@Test
	public void position_isCrossRiver_2(){
		//For B side, Pos 4 4 is CrossRiver
		Position pos = new Position(4,4);
		assertEquals(true, Position.isCrossRiver(Constants.PLAYER_B, pos));
	}
	
	@Test
	public void position_isCrossRiver_3(){
		//For A side, Pos 4 7 is CrossRiver
		Position pos = new Position(4,7);
		assertEquals(true, Position.isCrossRiver(Constants.PLAYER_A, pos));
	}

	@Test
	public void position_isCrossRiver_4(){
		//For B side, Pos 4 7 is not CrossRiver
		Position pos = new Position(4,7);
		assertEquals(false, Position.isCrossRiver(Constants.PLAYER_B, pos));
	}
	
	@Test
	public void position_isPositionInsidePossibleList_1(){
		//Pos 1 2 is inside the given list
		Position pos = new Position(1,2);
		
		ArrayList<Position> list = new ArrayList<Position>();
		list.add(new Position(1,2));
		list.add(new Position(2,2));
		
		assertEquals(true, Position.isPositionInsidePossibleList(pos, list));
	}
	
	@Test
	public void position_isPositionInsidePossibleList_2(){
		//Pos 1 1 is not in the list, but the X is in the list
		Position pos = new Position(1,1);
		
		ArrayList<Position> list = new ArrayList<Position>();
		list.add(new Position(1,2));
		list.add(new Position(2,2));
		
		assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
	}
	
	@Test
	public void position_isPositionInsidePossibleList_3(){
		//Pos 0 2 is not in the list, but the Y is in the list
		Position pos = new Position(0,2);
		
		ArrayList<Position> list = new ArrayList<Position>();
		list.add(new Position(1,2));
		list.add(new Position(2,2));
		
		assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
	}
	
	@Test
	public void position_isPositionInsidePossibleList_4(){
		//Pos -1 -1 is not in the list
		Position pos = new Position(-1,-1);
		
		ArrayList<Position> list = new ArrayList<Position>();
		list.add(new Position(1,2));
		list.add(new Position(2,2));
		
		assertEquals(false, Position.isPositionInsidePossibleList(pos, list));
	}

}
