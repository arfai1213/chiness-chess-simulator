package ChessType;
import java.util.ArrayList;

import GlobalData.Constants;

public class Position {
	private int x;
	private int y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	public String toString(){
		return "[" + this.getX() + "," + this.getY()+"]";
	}
	
	public static int isInsideBoard(Position position) {
		if (position.getX() < 0 || position.getX() > Constants.MAX_X_INDEX) {
			return Constants.POSITION_X_INVALID;
		}
		if (position.getY() < 0 || position.getY() > Constants.MAX_Y_INDEX) {
			return Constants.POSITION_Y_INVALID;
		}
		return Constants.POSITION_VALID;
	}
	public static boolean isCrossRiver(int player, Position position) {
		if (player == Constants.PLAYER_A && position.getY() > 4)
			return true;
		if (player == Constants.PLAYER_B && position.getY() < 5)
			return true;
		return false;
	}
	public static boolean isPositionInsidePossibleList(Position position, ArrayList<Position> validPosition) {
		Position p;
		for (int i = 0; i < validPosition.size(); i++) {
			p = validPosition.get(i);
			if (p.getX() == position.getX() && p.getY() == position.getY()) { // Not the command position
				return true;
			}
		}
		return false;
	}
}
