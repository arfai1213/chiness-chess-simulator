package ChessType;

import java.util.ArrayList;
import java.util.Arrays;

import GlobalData.Constants;

public class Scholar extends NonCrossableChess {

	public Scholar(int owner) {
		super(Constants.NAME_SCHOLAR, owner);
	}

	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		ArrayList<Position> checkList = new ArrayList<Position>(Arrays.asList(new Position(getPosition().getX() + 1, getPosition().getY() + 1), new Position(getPosition().getX() - 1, getPosition().getY() + 1), new Position(getPosition().getX() + 1, getPosition().getY() - 1), new Position(getPosition().getX() - 1, getPosition().getY() - 1)));
		Position p;
		for (int i = 0; i < checkList.size(); i++) {
			p = checkList.get(i);
			if (Position.isInsideBoard(p) != Constants.POSITION_VALID)
				continue;
			if(p.getX() > 5)
				continue;
			if(p.getX() < 3)
				continue;
			if(p.getY() > 2 && getOwner() == Constants.PLAYER_A) 
				continue;
			if (p.getY() < 7 && getOwner() == Constants.PLAYER_B)
				continue;
			
			possibleMoveList.add(p);
		}
		return possibleMoveList;
	}

}
