package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;

public abstract class NonCrossableChess extends Chess {
	public NonCrossableChess(String name, int owner) {
		super(name, owner);
	}

	@Override
	public int validMovement(Position position) {
		if (Position.isInsideBoard(position) != Constants.POSITION_VALID) {
			return Constants.POSITION_OUT_OF_BOUND;
		}
		if (Position.isCrossRiver(getOwner(), position)) // Should not cross river
			return Constants.SHOULD_NOT_CROSS_RIVER;

		if (position.getX() == getPosition().getX() && position.getY() == getPosition().getY())
			return Constants.THIS_IS_ALREADY_YOUR_POSITION;
		
		ArrayList<Position> validPosition = getPossibleMove();
		if (!Position.isPositionInsidePossibleList(position, validPosition))
			return Constants.POSITION_INVALID;

		return Constants.MOVEMENT_VALID;
	}
}
