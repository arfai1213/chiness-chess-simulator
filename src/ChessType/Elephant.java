package ChessType;
import java.util.ArrayList;
import java.util.Arrays;

import GlobalData.Constants;

public class Elephant extends NonCrossableChess {
	public Elephant(int owner) {
		super(Constants.NAME_ELEPHANT, owner);
	}
	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		ArrayList<Position> returnMoveList = new ArrayList<Position>();
		
		//		Start point of old version
		//		int checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + 1, getPosition().getY() + 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//			possibleMoveList.add(new Position(getPosition().getX() + 2, getPosition().getY() + 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + 1, getPosition().getY() - 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//			possibleMoveList.add(new Position(getPosition().getX() + 2, getPosition().getY() - 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() - 1, getPosition().getY() + 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//			possibleMoveList.add(new Position(getPosition().getX() - 2, getPosition().getY() + 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() - 1, getPosition().getY() - 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//			possibleMoveList.add(new Position(getPosition().getX() - 2, getPosition().getY() - 2));
		//			
		//		End point of old version
		
		// 		Refactoring Start 
		ArrayList<Position> kickFootOffsetCheckList = new ArrayList<Position>(Arrays.asList(new Position(2, 2), new Position(2, -2), new Position(-2, 2), new Position(-2, -2)));

		int checkKickFootResult;
		for (int i = 0; i < kickFootOffsetCheckList.size(); i++) {
			checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + (kickFootOffsetCheckList.get(i).getX() / 2), getPosition().getY() +  (kickFootOffsetCheckList.get(i).getY() / 2)));
			if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
				possibleMoveList.add(new Position(getPosition().getX() + kickFootOffsetCheckList.get(i).getX(), getPosition().getY() + kickFootOffsetCheckList.get(i).getY()));
			
		}
		// 		Refactoring End
		Position pos;
		for(int i = 0; i < possibleMoveList.size(); i++) {
			pos = possibleMoveList.get(i);
			if (Position.isInsideBoard(pos) == Constants.POSITION_VALID) {
				returnMoveList.add(pos);
			}
		}

		return returnMoveList;
		
	}
}
