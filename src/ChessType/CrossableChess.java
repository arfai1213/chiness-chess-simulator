package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;
import GlobalData.GameData;

public abstract class CrossableChess extends Chess {
	public CrossableChess(String name, int owner) {
		super(name, owner);
	}

	@Override
	public int validMovement(Position position) {
		if (Position.isInsideBoard(position) != Constants.POSITION_VALID) {
			return Constants.POSITION_OUT_OF_BOUND;
		}
		if (position.getX() == getPosition().getX() && position.getY() == getPosition().getY())
			return Constants.THIS_IS_ALREADY_YOUR_POSITION;
		
		ArrayList<Position> validPosition = getPossibleMove();
		if (!Position.isPositionInsidePossibleList(position, validPosition))
			return Constants.POSITION_INVALID;
		
		return Constants.MOVEMENT_VALID;
	}
	public int canKillGeneral() {
		ArrayList<Position> possible_move = getPossibleMove();
		Chess targetGeneral;
		if (getOwner() == Constants.PLAYER_A)
			targetGeneral = GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL];
		else
			targetGeneral = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL];
		
		for (int i = 0; i < possible_move.size(); i++) {
			
			if (possible_move.get(i).getX() == targetGeneral.getPosition().getX() && possible_move.get(i).getY() == targetGeneral.getPosition().getY()) {
				return Constants.I_CAN_KILL_YOU;
			}
		}
		return Constants.I_CANT_KILL_YOU;
	}
}
