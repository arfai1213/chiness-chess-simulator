package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;

public class Cannon extends CrossableChess {
	public Cannon(int owner) {
		super(Constants.NAME_CANNON, owner);
	}

	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		//		Start point of old version
		//		//=============================================================
		//		// Scan possible x
		//		//=============================================================
		//		int position_has_chess_result;
		//		int second_position_has_chess_result;
		//		Position created_position;
		//		for (int i = getPosition().getX(); i >= 0; i--) {
		//			created_position = new Position(i, getPosition().getY());
		//			
		//			//this is cannon position
		//			if (created_position.getX() == getPosition().getX() && created_position.getY() == getPosition().getY())
		//				continue;
		//			
		//			position_has_chess_result = Chess.isPositionExistsAliveChess(created_position);
		//			//Dont Exists a chess
		//			if (position_has_chess_result == Constants.POSITION_HAS_NO_CHESS)
		//				possibleMoveList.add(created_position);
		//			else { // Exists a chess
		//				Position second_chess_position;
		//				for (int k = created_position.getX(); k >= 0; k--) {
		//					second_chess_position = new Position(k, created_position.getY());
		//					
		//					//this is the first chess position
		//					if (second_chess_position.getX() == created_position.getX() && second_chess_position.getY() == created_position.getY())
		//						continue;
		//					
		//					second_position_has_chess_result = Chess.isPositionExistsAliveChess(second_chess_position);
		//					if ((second_position_has_chess_result == Constants.PLAYER_A && getOwner() == Constants.PLAYER_B) || (second_position_has_chess_result == Constants.PLAYER_B && getOwner() == Constants.PLAYER_A)) {
		//						possibleMoveList.add(second_chess_position);
		//						break;
		//					}
		//				}
		//				break;
		//			}
		//		}
		//		for (int i = getPosition().getX(); i <= Constants.MAX_X_INDEX; i++) {
		//			created_position = new Position(i, getPosition().getY());
		//			
		//			//this is cannon position
		//			if (created_position.getX() == getPosition().getX() && created_position.getY() == getPosition().getY())
		//				continue;
		//			
		//			position_has_chess_result = Chess.isPositionExistsAliveChess(created_position);
		//			//Dont Exists a chess
		//			if (position_has_chess_result == Constants.POSITION_HAS_NO_CHESS)
		//				possibleMoveList.add(created_position);
		//			else { // Exists a chess
		//				Position second_chess_position;
		//				for (int k = created_position.getX(); k <= Constants.MAX_X_INDEX; k++) {
		//					second_chess_position = new Position(k, created_position.getY());
		//					
		//					//this is the first chess position
		//					if (second_chess_position.getX() == created_position.getX() && second_chess_position.getY() == created_position.getY())
		//						continue;
		//					second_position_has_chess_result = Chess.isPositionExistsAliveChess(second_chess_position);
		//					if ((second_position_has_chess_result == Constants.PLAYER_A && getOwner() == Constants.PLAYER_B) || (second_position_has_chess_result == Constants.PLAYER_B && getOwner() == Constants.PLAYER_A)) {
		//						possibleMoveList.add(second_chess_position);
		//						break;
		//					}
		//				}
		//				break;
		//			}
		//		}
		//		
		//		//=============================================================
		//		// Scan possible y
		//		//=============================================================
		//		for (int i = getPosition().getY(); i >= 0; i--) {
		//			created_position = new Position(getPosition().getX(), i);
		//			
		//			//this is cannon position
		//			if (created_position.getX() == getPosition().getX() && created_position.getY() == getPosition().getY())
		//				continue;
		//			
		//			position_has_chess_result = Chess.isPositionExistsAliveChess(created_position);
		//			//Dont Exists a chess
		//			if (position_has_chess_result == Constants.POSITION_HAS_NO_CHESS)
		//				possibleMoveList.add(created_position);
		//			else { // Exists a chess
		//				Position second_chess_position;
		//				for (int k = created_position.getY(); k >= 0; k--) {
		//					second_chess_position = new Position(created_position.getX(), k);
		//					
		//					//this is the first chess position
		//					if (second_chess_position.getX() == created_position.getX() && second_chess_position.getY() == created_position.getY())
		//						continue;
		//					
		//					second_position_has_chess_result = Chess.isPositionExistsAliveChess(second_chess_position);
		//					if ((second_position_has_chess_result == Constants.PLAYER_A && getOwner() == Constants.PLAYER_B) || (second_position_has_chess_result == Constants.PLAYER_B && getOwner() == Constants.PLAYER_A)) {
		//						possibleMoveList.add(second_chess_position);
		//						break;
		//					}
		//				}
		//				break;
		//			}
		//		}
		//		for (int i = getPosition().getY(); i <= Constants.MAX_Y_INDEX; i++) {
		//			created_position = new Position(getPosition().getX(), i);
		//			
		//			//this is cannon position
		//			if (created_position.getX() == getPosition().getX() && created_position.getY() == getPosition().getY())
		//				continue;
		//			
		//			position_has_chess_result = Chess.isPositionExistsAliveChess(created_position);
		//			//Dont Exists a chess
		//			if (position_has_chess_result == Constants.POSITION_HAS_NO_CHESS)
		//				possibleMoveList.add(created_position);
		//			else { // Exists a chess
		//				Position second_chess_position;
		//				for (int k = created_position.getY(); k <= Constants.MAX_Y_INDEX; k++) {
		//					second_chess_position = new Position(created_position.getX(), k);
		//					
		//					//this is the first chess position
		//					if (second_chess_position.getX() == created_position.getX() && second_chess_position.getY() == created_position.getY())
		//						continue;
		//					
		//					second_position_has_chess_result = Chess.isPositionExistsAliveChess(second_chess_position);
		//					if ((second_position_has_chess_result == Constants.PLAYER_A && getOwner() == Constants.PLAYER_B) || (second_position_has_chess_result == Constants.PLAYER_B && getOwner() == Constants.PLAYER_A)) {
		//						possibleMoveList.add(second_chess_position);
		//						break;
		//					}
		//				}
		//				break;
		//			}
		//		}
		//		End point of old version
		
		//		Refactoring Start 
		
		//=============================================================
		// Scan possible x
		//=============================================================

		Position created_position;
		boolean stopSearchDirectMove = false;
		for (int i = getPosition().getX() - 1; i >= 0; i--) {
			created_position = new Position(i, getPosition().getY());
			if (!stopSearchDirectMove) {
				if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP) {
					stopSearchDirectMove = true;
				}
			}
			else {
				if (Chess.isPositionExistsAliveChess(created_position) != Constants.POSITION_HAS_NO_CHESS) {
					possibleMoveList.add(created_position);
					break;
				}
			}
		}
		stopSearchDirectMove = false;
		for (int i = getPosition().getX() + 1; i <= Constants.MAX_X_INDEX; i++) {
			created_position = new Position(i, getPosition().getY());
			if (!stopSearchDirectMove) {
				if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP) {
					stopSearchDirectMove = true;
				}
			}
			else {
				if (Chess.isPositionExistsAliveChess(created_position) != Constants.POSITION_HAS_NO_CHESS) {
					possibleMoveList.add(created_position);
					break;
				}
			}
		}
		
		//=============================================================
		// Scan possible y
		//=============================================================
		stopSearchDirectMove = false;
		for (int i = getPosition().getY() - 1; i >= 0; i--) {
			created_position = new Position(getPosition().getX(), i);
			if (!stopSearchDirectMove) {
				if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP) {
					stopSearchDirectMove = true;
				}
			}
			else {
				if (Chess.isPositionExistsAliveChess(created_position) != Constants.POSITION_HAS_NO_CHESS) {
					possibleMoveList.add(created_position);
					break;
				}
			}
		}
		stopSearchDirectMove = false;
		for (int i = getPosition().getY() + 1; i <= Constants.MAX_Y_INDEX; i++) {
			created_position = new Position(getPosition().getX(), i);
			if (!stopSearchDirectMove) {
				if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP) {
					stopSearchDirectMove = true;
				}
			}
			else {
				if (Chess.isPositionExistsAliveChess(created_position) != Constants.POSITION_HAS_NO_CHESS) {
					possibleMoveList.add(created_position);
					break;
				}
			}
		}
		
		return possibleMoveList;
	}
	public int tryAddDirectMovePosition(ArrayList<Position> possibleMoveList, Position created_position) {
		int position_has_chess_result = Chess.isPositionExistsAliveChess(created_position);
		//Dont Exists a chess
		if (position_has_chess_result == Constants.POSITION_HAS_NO_CHESS) {
			possibleMoveList.add(created_position);
			return Constants.ADD_POSITION_CONTINUE;
		}
		return Constants.ADD_POSITION_STOP;
	}
	//		Refactoring End
}
