package ChessType;

import java.util.ArrayList;
import java.util.Arrays;

import GlobalData.Constants;

public class Horse extends CrossableChess {
	public Horse(int owner) {
		super(Constants.NAME_HORSE, owner);
	}

	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		ArrayList<Position> returnPossibleList = new ArrayList<Position>();

		//		Start point of old version
		//		int checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX(), getPosition().getY() + 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//			possibleMoveList.add(new Position(getPosition().getX() + 1, getPosition().getY() + 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX(), getPosition().getY() + 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() - 1, getPosition().getY() + 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX(), getPosition().getY() - 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() + 1, getPosition().getY() - 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX(), getPosition().getY() - 1));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() - 1, getPosition().getY() - 2));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + 1, getPosition().getY()));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() + 2, getPosition().getY() + 1));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + 1, getPosition().getY()));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() + 2, getPosition().getY() - 1));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() - 1, getPosition().getY()));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() - 2, getPosition().getY() + 1));
		//		
		//		checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() - 1, getPosition().getY()));
		//		if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
		//		possibleMoveList.add(new Position(getPosition().getX() - 2, getPosition().getY() - 1));

		//		End point of old version
		
		
		// 		Refactoring Start 
		ArrayList<Position> kickFootOffsetCheckList = new ArrayList<Position>(Arrays.asList(new Position(1, 2), new Position(-1, 2), new Position(1, -2), new Position(-1, - 2), new Position(2, 1), new Position(2, -1), new Position(-2, 1), new Position(-2, -1)));

		int checkKickFootResult;
		for (int i = 0; i < kickFootOffsetCheckList.size(); i++) {
			checkKickFootResult = Chess.isPositionExistsAliveChess(new Position(getPosition().getX() + (kickFootOffsetCheckList.get(i).getX() / 2), getPosition().getY() +  (kickFootOffsetCheckList.get(i).getY() / 2)));
			if (checkKickFootResult == Constants.POSITION_HAS_NO_CHESS || checkKickFootResult == Constants.POSITION_HAS_DEAD_CHESS)
				possibleMoveList.add(new Position(getPosition().getX() + kickFootOffsetCheckList.get(i).getX(), getPosition().getY() + kickFootOffsetCheckList.get(i).getY()));
			
		}
		// 		Refactoring End
		Position p;
		for (int i = 0; i < possibleMoveList.size(); i++) {
			p = possibleMoveList.get(i);
			if (Position.isInsideBoard(p) == Constants.POSITION_VALID) {
				returnPossibleList.add(p);
			}
		}
		return returnPossibleList;
		
	}
}
