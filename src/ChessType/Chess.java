package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;
import GlobalData.GameData;

public abstract class Chess {
	private boolean isAlive;
	private Position position;
	private int owner;
	private String name;

	public Chess(String name, int owner) {
		this.name = name;
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public void setPosition(int x, int y) {
		this.position = new Position(x, y);
	}

	public int goTo(Position position) {
		if (!isAlive()) {
			return Constants.CHESS_IS_NOT_ALIVE;
		}
		int valid_checking_result = validMovement(position);
		if (valid_checking_result != Constants.MOVEMENT_VALID) {
			return valid_checking_result;
		}
		// if the movement if valid
		int position_has_chess_result = Chess.isPositionExistsAliveChess(position);
		if (position_has_chess_result != Constants.POSITION_HAS_NO_CHESS) { // need to eat / avoid moving
			// if the position has the alive chess
			if (position_has_chess_result != Constants.POSITION_HAS_DEAD_CHESS) {
				if (position_has_chess_result == getOwner()) // Cant move the chess to the position which exists the own chess.
					return Constants.POSITION_HAS_OWN_CHESS;
				
				// if the existed chess is the enermy
				Chess.killEnermy(position);
			}
			
			// Position has other chess
		}
		setPosition(position.getX(), position.getY());
		return Constants.CHESS_MOVED_SUCCESS;
	}

	public abstract int validMovement(Position position);
	public abstract ArrayList<Position> getPossibleMove();
	public void die() {
		isAlive = false;
	}
	public void respawn() {
		isAlive = true;
	}
	public boolean isAlive() {
		return isAlive;
	}

	public int getOwner() {
		return owner;
	}

	public static int isPositionExistsAliveChess(Position position) {
		for (int i = 0; i < GameData.chessList.length; i++) {
			for (Chess c : GameData.chessList[i]) {
				if (position.getX() == c.getPosition().getX() && position.getY() == c.getPosition().getY()) {
					return (c.isAlive()) ? c.getOwner() : Constants.POSITION_HAS_DEAD_CHESS;
				}
			}
		}
		return Constants.POSITION_HAS_NO_CHESS;
	}
	public static void killEnermy(Position position) {
		for (int i = 0; i < GameData.chessList.length; i++) {
			for (Chess c : GameData.chessList[i]) {
				if (position.getX() == c.getPosition().getX() && position.getY() == c.getPosition().getY()) {
					c.die();
					if (c.getName().equals(Constants.NAME_GENERAL)) {
						if (c.getOwner() == Constants.PLAYER_A)
							System.out.println("Player 1 Lose.\nPlayer 2 Win.");
						else
							System.out.println("Player 1 Win.\nPlayer 2 Lose.");
					}
					return;
				}
			}
		}
	}
}