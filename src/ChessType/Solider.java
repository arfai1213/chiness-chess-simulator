package ChessType;
import java.util.ArrayList;

import GlobalData.Constants;


public class Solider extends CrossableChess {
	public Solider(int owner) {
		super(Constants.NAME_SOLIDER, owner);
	}
	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();

		// This is the old version before refactoring  Start Point
		//		if (Position.isCrossRiver(getOwner(), getPosition())) {
		//			
		//			possibleMoveList.add(new Position(getPosition().getX() + 1, getPosition().getY()));
		//			possibleMoveList.add(new Position(getPosition().getX() - 1, getPosition().getY()));
		//			if (getOwner() == Constants.PLAYER_A)
		//				possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() + 1));
		//			else
		//				possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() - 1));
		//			
		//		}
		//		else {
		//			if (getOwner() == Constants.PLAYER_A)
		//				possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() + 1));
		//			else
		//				possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() - 1));
		//		}
		//		
		
		// This is the old version before refactoring  End Point

		// New version after refactoring  Start Point
		if (Position.isCrossRiver(getOwner(), getPosition())) {
			
			possibleMoveList.add(new Position(getPosition().getX() + 1, getPosition().getY()));
			possibleMoveList.add(new Position(getPosition().getX() - 1, getPosition().getY()));
		
			
		}
		if (getOwner() == Constants.PLAYER_A)
			possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() + 1));
		else
			possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() - 1));
		
		// New version after refactoring  End Point
		Position p;
		for (int i = 0; i < possibleMoveList.size(); i++) {
			p = possibleMoveList.get(i);
			if (Position.isInsideBoard(p) != Constants.POSITION_VALID) {
				possibleMoveList.remove(p);
			}
		}
		return possibleMoveList;
		
	}

}
