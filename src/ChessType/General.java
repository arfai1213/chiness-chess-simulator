package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;
import GlobalData.GameData;

public class General extends CrossableChess {
	public General(int owner) {
		super(Constants.NAME_GENERAL, owner);
	}
	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		possibleMoveList.add(new Position(getPosition().getX() + 1, getPosition().getY()));
		possibleMoveList.add(new Position(getPosition().getX() - 1, getPosition().getY()));
		possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() + 1));
		possibleMoveList.add(new Position(getPosition().getX(), getPosition().getY() - 1));
		Position p;

		for (int i = 0; i < possibleMoveList.size(); i++) {
			p = possibleMoveList.get(i);
			if (Position.isInsideBoard(p) != Constants.POSITION_VALID) {
				possibleMoveList.remove(p);
			}
			if(p.getX() > 5)
				possibleMoveList.remove(p);
			if(p.getX() < 3)
				possibleMoveList.remove(p);
			if((p.getY() > 2 && getOwner() == Constants.PLAYER_A))
				possibleMoveList.remove(p);
			if((p.getY() < 7 && getOwner() == Constants.PLAYER_B))
				possibleMoveList.remove(p);
		}
		
		if (GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL].getPosition().getX() == GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL].getPosition().getX()) {
			boolean exists_other_chess = false;
			int xPos = GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL].getPosition().getX();
			for (int i = 0; i < GameData.chessList.length; i++) {
				for (Chess c : GameData.chessList[i]) {
					if (!c.isAlive())
							continue;
					if (c.getPosition().getX() == xPos && !c.getName().equals(Constants.NAME_GENERAL)) {
						exists_other_chess = true;
					}
				}
			}
			if (!exists_other_chess) {
				if (getOwner() == Constants.PLAYER_A) {
					possibleMoveList.add(GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL].getPosition());
				}
				else {
					possibleMoveList.add(GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL].getPosition());
				}
			}
		}
		System.out.println(possibleMoveList);
		return possibleMoveList;
	}
}
