package ChessType;

import java.util.ArrayList;

import GlobalData.Constants;

public class Chariot extends CrossableChess {
	public Chariot(int owner) {
		super(Constants.NAME_CHARIOT, owner);
	}

	@Override
	public ArrayList<Position> getPossibleMove() {
		ArrayList<Position> possibleMoveList = new ArrayList<Position>();
		//=============================================================
		// Scan possible x
		//=============================================================
		
		Position created_position;
		for (int i = getPosition().getX() - 1; i >= 0; i--) {
			created_position = new Position(i, getPosition().getY());
			if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP)
				break;
		}
		for (int i = getPosition().getX() + 1; i <= Constants.MAX_X_INDEX; i++) {
			created_position = new Position(i, getPosition().getY());
			if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP)
				break;
		}
		
		//=============================================================
		// Scan possible y
		//=============================================================
		for (int i = getPosition().getY() - 1; i >= 0; i--) {
			created_position = new Position(getPosition().getX(), i);
			if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP)
				break;
		}
		
		for (int i = getPosition().getY() + 1; i <= Constants.MAX_Y_INDEX; i++) {
			created_position = new Position(getPosition().getX(), i);
			if (tryAddDirectMovePosition(possibleMoveList, created_position) == Constants.ADD_POSITION_STOP)
				break;
		}
		
		return possibleMoveList;
	}
	public int tryAddDirectMovePosition(ArrayList<Position> possibleMoveList, Position created_position) {
		
		possibleMoveList.add(created_position);
		//Exists a chess
		if (Chess.isPositionExistsAliveChess(created_position) != Constants.POSITION_HAS_NO_CHESS)
			return Constants.ADD_POSITION_STOP;
		return Constants.ADD_POSITION_CONTINUE;
	}

}
