package Run;

import javax.imageio.ImageIO;
import javax.swing.*;

import ChessType.Chess;
import GlobalData.Constants;
import GlobalData.GameData;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.geom.*;
import java.io.InputStream;

public class ChessUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2052466444192572875L;
	private Chess[] player1;
	private Chess[] player2;
	
	private JPanel mainPanel;
	private JPanel boardPanel;
	private JPanel consolePanel;
	
	private String systemMsg=new String();
	private final JLabel jLabel;
	private final JTextArea jTextArea;
	final JTextField jTextField;
	
	private final int prefixX=20;
	private final int prefixY=50;
	
	public ChessUI(Chess[][] chessList){
		
		this.player1=chessList[0];
        this.player2=chessList[1];
        
		setupPanel();
		
        jLabel = new JLabel(); 
        jLabel.setBounds(20,20,100,40);

        jTextField = new JTextField(40);
        appendLine();
        appendMsg("Game Start");
        appendLine();

        jTextArea = new JTextArea(systemMsg,25 ,40);
        jTextArea.setLineWrap(true);
        
        /*if (GameData.currentTurn == Constants.PLAYER_A)
        	jLabel.setText("Player 1 Turn");
        else
        	jLabel.setText("Player 2 Turn");*/
        
        jTextField.addActionListener(new AbstractAction()
        {
			private static final long serialVersionUID = 7197848959465611048L;

			@Override
            public void actionPerformed(ActionEvent e) {
            	String command = jTextField.getText();
        		String[] commands = command.split(" ");
                int result = CommandHandler.receiveCommand(command);
                if (result == Constants.COMMAND_SUCCESS) { // 
                	if (GameData.currentTurn == Constants.PLAYER_A) {
                		GameData.currentTurn = Constants.PLAYER_B;
                		appendMsg("Chess("+commands[0]+","+commands[1]+") go to Position("+commands[2]+","+commands[3]+") Successfully");
                		appendMsg("Player 2 Turn");
                	}
                	else {
                		GameData.currentTurn = Constants.PLAYER_A;
                		appendMsg("Chess("+commands[0]+","+commands[1]+") go to Position("+commands[2]+","+commands[3]+") Successfully");
                		appendMsg("Player 1 Turn");
                	}
                	repaint();
                	
                }
                else {	// error
                	appendMsg("Input Error ! \nBecause "+Constants.getSystemMessageById(result)+"!\nPlease try again.");
                	repaint();
                }
            }

        });
        //consolePanel.add(jLabel);
        //jLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        //consolePanel.add(jTextField);
        
        //jTextArea.setPreferredSize(new Dimension(100, 100));
        JScrollPane scrollPane = new JScrollPane(jTextArea);
        consolePanel.add(scrollPane);
        consolePanel.add(jTextField);
	}
	
	public void paint(Graphics g){
		super.paint(g);  // fixes the immediate problem.
        Graphics2D g2 = (Graphics2D) g;
        g.clearRect(0, 0, prefixX+550, prefixY+550);
        drawBoard(g2);
        for(int i=0;i<player1.length;i++){
        	if (this.player1[i].isAlive())
        		drawChess(g, this.player1[i]);
        }
        for(int i=0;i<player2.length;i++){
        	if (this.player2[i].isAlive())
        	drawChess(g,this.player2[i]);
        }
        drawIndex(g);
        jTextArea.setText(systemMsg);
	}
	
	private void setupPanel(){
		setSize(prefixX+550+550,prefixY+550);
		
		mainPanel=new JPanel();
		mainPanel.setLayout(new GridLayout(1,2));
		
        boardPanel=new JPanel();
        mainPanel.add(boardPanel);
        
        consolePanel=new JPanel();
        mainPanel.add(consolePanel);
        //consolePanel.setLayout(new BoxLayout(consolePanel, BoxLayout.Y_AXIS));	
        consolePanel.setLayout(new FlowLayout());
        
        getContentPane().add(mainPanel);
	}
	
	private void drawBoard(Graphics2D g2){
		for(int i=0;i<9;i++){
        	Line2D topLin = new Line2D.Float(prefixX+55+i*55, prefixY+50, prefixX+55+i*55, prefixY+250);
        	Line2D botLin = new Line2D.Float(prefixX+55+i*55, prefixY+300, prefixX+55+i*55, prefixY+500);
        	g2.draw(topLin);
        	g2.draw(botLin);
        }
        for(int i=0;i<10;i++){
        	Line2D lin=new Line2D.Float(prefixX+55, prefixY+50+i*50, prefixX+495, prefixY+50+i*50);
        	g2.draw(lin);
        }
        Line2D topLin = new Line2D.Float(prefixX+55, prefixY+50, prefixX+55, prefixY+500);
    	Line2D botLin = new Line2D.Float(prefixX+495, prefixY+50, prefixX+495, prefixY+500);
    	g2.draw(topLin);
    	g2.draw(botLin);
    	Line2D crossLin_black_1 = new Line2D.Float(prefixX+220, prefixY+50, prefixX+330, prefixY+150);
    	Line2D crossLin_black_2 = new Line2D.Float(prefixX+220, prefixY+150, prefixX+330, prefixY+50);
    	g2.draw(crossLin_black_1);
    	g2.draw(crossLin_black_2);
    	Line2D crossLin_red_1 = new Line2D.Float(prefixX+220, prefixY+500, prefixX+330, prefixY+400);
    	Line2D crossLin_red_2 = new Line2D.Float(prefixX+220, prefixY+400, prefixX+330, prefixY+500);
    	g2.draw(crossLin_red_1);
    	g2.draw(crossLin_red_2);
	}
	
	private void drawChess(Graphics g, Chess c){		
		String imgSrc= "/resource/"+c.getName().toLowerCase();
		imgSrc +=  "_"+c.getOwner()+".png";
		InputStream is = this.getClass().getResourceAsStream(imgSrc);
		int x=this.convertLogicXToX(c.getPosition().getX());
		int y=this.convertLogicYToY(c.getPosition().getY());
		try{
			Image image = ImageIO.read(is);
            g.drawImage(image, x, y, null);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private String getLine(int length){
		String result=new String();
		for(int i=0;i<length;i++){
			result+="=";
		}
		return result;
	}
	
	private void appendMsg(String msg){
		systemMsg+="\n"+msg;
	}
	
	private void appendLine(){
		appendMsg(getLine(60));
	}
	
	private void drawIndex(Graphics g){
		g.setFont(new Font("TimesRoman", Font.PLAIN, 30)); 
		for(int i=0;i<9;i++){
			g.drawString(Integer.toString(i), convertLogicXToX(i)+20, 70);
		}
		for(int i=0;i<10;i++){
			g.drawString(Integer.toString(i), 20, convertLogicYToY(i)+30);
		}
	}
	
	private int convertLogicXToX(int x){
		return prefixX+55+x*55-27;
	}
	
	private int convertLogicYToY(int y){
		return prefixY+50+y*50-25;
	}
}
