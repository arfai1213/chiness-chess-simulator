package Run;

import GlobalData.Constants;
import GlobalData.GameData;
import ChessType.*;

public class CommandHandler {
	public static int receiveCommand(String original_command) {
		String[] commandString = original_command.split(" ");
		if (commandString.length != 4) {
			// this part will be removed
			if (commandString.length == 2)
			{
				int[] command = new int[2];
				try {
					for (int i = 0; i < command.length; i++) {
						command[i] = Integer.parseInt(commandString[i]);
						if (command[i] < 0)
							return Constants.COMMAND_INTEGER_INVALID;
						else {
							if (i == 0 && command[i] > Constants.MAX_X_INDEX) // should be position-x
								return Constants.COMMAND_INTEGER_INVALID;
							else if (command[i] > Constants.MAX_Y_INDEX)
								return Constants.COMMAND_INTEGER_INVALID;
						}
					}
				}
				catch (Exception ex) {
					return Constants.COMMAND_NOT_INTEGER;
				}

				// Command checking is passed
				Chess selectedChess = null;
				for (int i = 0; i < GameData.chessList.length && selectedChess == null; i++) {
					for (Chess c : GameData.chessList[i]){
						if (c.getPosition().getX() == command[0] && c.getPosition().getY() == command[1]) {
							selectedChess = c;
							break;
						}
					}
				}
				if (selectedChess == null) // No any chess found after searching all the chess
					return Constants.NO_THIS_CHESS;
				
				System.out.println("Possible move of "+ command[0] +" , " + command[1]);
				for (Position p : selectedChess.getPossibleMove()) {
					System.out.println(p.getX() + " , " + p.getY());
				}
				
			}
			// this part will be removed
			return Constants.COMMAND_PARAM_INVALID;
		}
		int[] command = new int[4];
		try {
			for (int i = 0; i < command.length; i++) {
				command[i] = Integer.parseInt(commandString[i]);
				if (command[i] < 0)
					return Constants.COMMAND_INTEGER_INVALID;
				else {
					if (i == 0  && command[i] > Constants.MAX_X_INDEX) // should be position-x
						return Constants.COMMAND_INTEGER_INVALID;
					else if (command[i] > Constants.MAX_Y_INDEX)
						return Constants.COMMAND_INTEGER_INVALID;
				}
			}
		}
		catch (Exception ex) {
			return Constants.COMMAND_NOT_INTEGER;
		}

		// Command checking is passed
		Chess selectedChess = null;
		for (int i = 0; i < GameData.chessList.length && selectedChess == null; i++) {
			for (Chess c : GameData.chessList[i]){
				if (c.getPosition().getX() == command[0] && c.getPosition().getY() == command[1]) {
					selectedChess = c;
					break;
				}
			}
		}
		if (selectedChess == null) // No any chess found after searching all the chess
			return Constants.NO_THIS_CHESS;
		
		// Command mentioned position has a chess
		if ((GameData.currentTurn == Constants.PLAYER_A && selectedChess.getOwner() == Constants.PLAYER_B) || (GameData.currentTurn == Constants.PLAYER_B && selectedChess.getOwner() == Constants.PLAYER_A))
			return Constants.NOT_CHESS_OWNER;
		
		// The chess owner is playing the turn
		int movement_result = selectedChess.goTo(new Position(command[2], command[3]));
		
		if (movement_result != Constants.CHESS_MOVED_SUCCESS) { // if the movement failed
			return movement_result;
		}
		// if the movement success
		return Constants.COMMAND_SUCCESS;
	}
}
