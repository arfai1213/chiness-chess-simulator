package Run;

import ChessType.Cannon;
import ChessType.Chariot;
import ChessType.Chess;
import ChessType.Elephant;
import ChessType.General;
import ChessType.Horse;
import ChessType.Scholar;
import ChessType.Solider;
import GlobalData.*;
public class Main {
	public static void main(String[] args) {
		// should have 2 players.

		createGameObject();
		initGame();
	   
		GameData.chessUI=new ChessUI(GameData.chessList);
		GameData.chessUI.setVisible(true);
        
	}
	public static void createGameObject() {
		GameData.chessList = new Chess[2][16];
		for (int i = Constants.PLAYER_A; i < Constants.PLAYER_B + 1; i++) {
			GameData.chessList[i][Constants.INDEX_SOLIDER_1] = new Solider(i);
			GameData.chessList[i][Constants.INDEX_SOLIDER_2] = new Solider(i);
			GameData.chessList[i][Constants.INDEX_SOLIDER_3] = new Solider(i);
			GameData.chessList[i][Constants.INDEX_SOLIDER_4] = new Solider(i);
			GameData.chessList[i][Constants.INDEX_SOLIDER_5] = new Solider(i);
			GameData.chessList[i][Constants.INDEX_CANNON_1] = new Cannon(i);
			GameData.chessList[i][Constants.INDEX_CANNON_2] = new Cannon(i);
			GameData.chessList[i][Constants.INDEX_CHARIOT_1] = new Chariot(i);
			GameData.chessList[i][Constants.INDEX_CHARIOT_2] = new Chariot(i);
			GameData.chessList[i][Constants.INDEX_HORSE_1] = new Horse(i);
			GameData.chessList[i][Constants.INDEX_HORSE_2] = new Horse(i);
			GameData.chessList[i][Constants.INDEX_ELEPHANT_1] = new Elephant(i);
			GameData.chessList[i][Constants.INDEX_ELEPHANT_2] = new Elephant(i);
			GameData.chessList[i][Constants.INDEX_SCHOLAR_1] = new Scholar(i);
			GameData.chessList[i][Constants.INDEX_SCHOLAR_2] = new Scholar(i);
			GameData.chessList[i][Constants.INDEX_GENERAL] = new General(i);
		}
	}
	public static void initGame() {
	    GameData.currentTurn = Constants.PLAYER_A;
		// Init Chess data - Player 1
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_1].setPosition(0, 3);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_2].setPosition(2, 3);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_3].setPosition(4, 3);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_4].setPosition(6, 3);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SOLIDER_5].setPosition(8, 3);
		
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_1].setPosition(1, 2);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CANNON_2].setPosition(7, 2);
		
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_1].setPosition(0, 0);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_CHARIOT_2].setPosition(8, 0);
		
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_1].setPosition(1, 0);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_HORSE_2].setPosition(7, 0);
		
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_1].setPosition(2, 0);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_ELEPHANT_2].setPosition(6, 0);
		
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_1].setPosition(3, 0);
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_SCHOLAR_2].setPosition(5, 0);
		// General
	    GameData.chessList[Constants.PLAYER_A][Constants.INDEX_GENERAL].setPosition(4, 0);
		
		// Init Chess data - Player 2
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_1].setPosition(0, 6);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_2].setPosition(2, 6);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_3].setPosition(4, 6);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_4].setPosition(6, 6);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SOLIDER_5].setPosition(8, 6);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_1].setPosition(1, 7);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CANNON_2].setPosition(7, 7);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_1].setPosition(0, 9);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_CHARIOT_2].setPosition(8, 9);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_1].setPosition(1, 9);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_HORSE_2].setPosition(7, 9);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_1].setPosition(2, 9);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_ELEPHANT_2].setPosition(6, 9);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SCHOLAR_1].setPosition(3, 9);
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_SCHOLAR_2].setPosition(5, 9);
		
	    GameData.chessList[Constants.PLAYER_B][Constants.INDEX_GENERAL].setPosition(4, 9);
	    
	    for (int i = 0; i < GameData.chessList.length; i++) {
	    	for (Chess c : GameData.chessList[i]) {
	    		c.respawn();
	    	}
	    }
	}
}
