package GlobalData;

public class Constants {
	// Game Config
	public static final int MAX_X_INDEX 						= 8;
	public static final int MAX_Y_INDEX 						= 9;
	
	// Uni Id
	public static final int INVALID 					= -1;
	
	public static final int ADD_POSITION_CONTINUE		= 0;
	public static final int ADD_POSITION_STOP	= 1;
	
	public static final int PLAYER_A 					= 0;
	public static final int PLAYER_B 					= 1;
	
	public static final int CHESS_IS_NOT_ALIVE 				= 2;
	public static final int NOT_CHESS_OWNER					= 3;
	public static final int CHESS_MOVED_SUCCESS 			= 4;
	public static final int MOVEMENT_VALID 					= 5;
	public static final int POSITION_OUT_OF_BOUND 			= 6;
	public static final int SHOULD_NOT_CROSS_RIVER 			= 7;
	public static final int POSITION_INVALID 				= 8;
	public static final int NO_THIS_CHESS 					= 9;
	public static final int COMMAND_PARAM_INVALID 			= 10;
	public static final int COMMAND_INTEGER_INVALID 		= 11;
	public static final int COMMAND_NOT_INTEGER 			= 12;
	public static final int COMMAND_INVALID 				= 13;
	public static final int COMMAND_SUCCESS 				= 14;
	public static final int COMMAND_MOVED_FAILED 			= 15;
	public static final int POSITION_X_INVALID 				= 16;
	public static final int POSITION_Y_INVALID 				= 17;
	public static final int POSITION_VALID 					= 18;
	public static final int POSITION_HAS_NO_CHESS 			= 19;
	public static final int POSITION_HAS_OWN_CHESS			= 20;
	public static final int POSITION_HAS_DEAD_CHESS 		= 21;
	public static final int THIS_IS_ALREADY_YOUR_POSITION 	= 22;
	public static final int I_CAN_KILL_YOU 					= 23;
	public static final int I_CANT_KILL_YOU					= 24;
	
	// Array Config
	public static final int INDEX_SOLIDER_1 			= 0;
	public static final int INDEX_SOLIDER_2 			= 1;
	public static final int INDEX_SOLIDER_3				= 2;
	public static final int INDEX_SOLIDER_4				= 3;
	public static final int INDEX_SOLIDER_5				= 4;
	public static final int INDEX_CANNON_1				= 5;
	public static final int INDEX_CANNON_2				= 6;
	public static final int INDEX_CHARIOT_1				= 7;
	public static final int INDEX_CHARIOT_2				= 8;
	public static final int INDEX_HORSE_1				= 9;
	public static final int INDEX_HORSE_2				= 10;
	public static final int INDEX_ELEPHANT_1			= 11;
	public static final int INDEX_ELEPHANT_2			= 12;
	public static final int INDEX_SCHOLAR_1				= 13;
	public static final int INDEX_SCHOLAR_2				= 14;
	public static final int INDEX_GENERAL				= 15;
	
	public static final String NAME_SOLIDER = "Solider";
	public static final String NAME_CANNON = "Cannon";
	public static final String NAME_CHARIOT = "Chariot";
	public static final String NAME_HORSE = "Horse";
	public static final String NAME_ELEPHANT = "Elephant";
	public static final String NAME_SCHOLAR = "Scholar";
	public static final String NAME_GENERAL = "General";
	
	public static String getSystemMessageById(int id){
		String result="";
		switch(id){
		case 2:
			result="chess is not alive";
			break;
		case 3:
			result="you are not the chess owner";
			break;
		case 4:
			result="move successfully";
			break;
		case 5:
			result="movement Valid";
			break;
		case 6:
			result="position is not inside the chess board";
			break;
		case 7:
			result="the chess can not cross the river";
			break;
		case 8:
			result="position is invalid";
			break;
		case 9:
			result="there is no such chess on the position";
			break;
		case 10:
			result="the parameters you have entered, has problem";
			break;
		case 11:
			result="integer you entered is invalid";
			break;
		case 12:
			result="the command you enter is not integer";
			break;
		case 13:
			result="the command you enter is not valid";
			break;
		case 14:
			result="command success";
			break;
		case 15:
			result="the move is fail";
			break;
		case 16:
			result="position x is not valid";
			break;
		case 17:
			result="it is not valid";
			break;
		case 18:
			result="position is valid";
			break;
		case 19:
			result="the is no chess on the position";
			break;
		case 20:
			result="there is a chess on the position";
			break;
		case 21:
			result="there is a dead chess on the position";
			break;
		case 22:
			result="you haven't move";
			break;
		case 23:
			result="I can kill you";
			break;
		case 24:
			result="I can't kill you";
			break;
		}
		return result;
	}
	
}
